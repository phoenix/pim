
      SUBROUTINE GIVENS(A,B,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      COMPLEX A,B,C,S
*     ..
*     .. Local Scalars ..
      COMPLEX TAU
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      COMPLEX CZERO
      PARAMETER (CZERO= (0.0,0.0))
      COMPLEX CONE
      PARAMETER (CONE= (1.0,0.0))
*     ..
      IF (B.EQ.CZERO) THEN
          C = CONE
          S = CZERO

      ELSE IF (ABS(B).GT.ABS(A)) THEN
          TAU = -A/B
          S = CONE/SQRT(CONE+TAU**2)
          C = S*TAU

      ELSE
          TAU = -B/A
          C = CONE/SQRT(CONE+TAU**2)
          S = C*TAU
      END IF

      RETURN

      END
