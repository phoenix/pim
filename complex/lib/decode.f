
      SUBROUTINE DECODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      COMPLEX C,RHO,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      REAL ONE
      PARAMETER (ONE=1.0)
      COMPLEX CZERO
      PARAMETER (CZERO= (0.0,0.0))
      COMPLEX CONE
      PARAMETER (CONE= (1.0,0.0))
*     ..
      IF (RHO.EQ.CONE) THEN
          C = CZERO
          S = CONE

      ELSE IF (ABS(RHO).LT.ONE) THEN
          S = 2.0*RHO
          C = SQRT(CONE-S**2)

      ELSE
          C = 2.0/RHO
          S = SQRT(CONE-C**2)
      END IF

      RETURN

      END
