
      SUBROUTINE ENCODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      COMPLEX C,RHO,S
*     ..
*     .. External Functions ..
      COMPLEX CSIGN
      EXTERNAL CSIGN
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS
*     ..
*     .. Parameters ..
      COMPLEX CZERO
      PARAMETER (CZERO= (0.0,0.0))
      COMPLEX CONE
      PARAMETER (CONE= (1.0,0.0))
*     ..
      IF (C.EQ.CZERO) THEN
          RHO = CONE

      ELSE IF (ABS(S).LT.ABS(C)) THEN
          RHO = CSIGN(C)*S/2.0

      ELSE
          RHO = 2.0*CSIGN(S)/C
      END IF

      RETURN

      END
