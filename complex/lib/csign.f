
      COMPLEX FUNCTION CSIGN(X)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      COMPLEX X
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS
*     ..
      CSIGN = X/ABS(X)
      RETURN

      END
