      SUBROUTINE PIMCCHEBYSHEV(X,B,WRK,IPAR,SPAR,MATVEC,PRECONL,PRECONR,
     +                         PCSUM,PSCNRM,PROGRESS)
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      REAL ZERO
      PARAMETER (ZERO=0.0E0)
      COMPLEX CONE
      PARAMETER (CONE= (1.0E0,0.0E0))
      REAL ONE
      PARAMETER (ONE=1.0E0)
      REAL TWO
      PARAMETER (TWO=2.0)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
*     ..
*     .. Array Arguments ..
      COMPLEX B(*),WRK(*),X(*)
      REAL SPAR(SPARSIZ)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Function Arguments ..
      REAL PSCNRM
      EXTERNAL PSCNRM
*     ..
*     .. Subroutine Arguments ..
      EXTERNAL MATVEC,PCSUM,PRECONL,PRECONR,PROGRESS
*     ..
*     .. Local Scalars ..
      REAL AXISISQ,AXISRSQ,D,DELTA,EPSILON,EXITNORM,GAMMA,LENGTHI,
     +     LENGTHR,RHO,RHSSTOP,SIGMA,SIGMASQ
      INTEGER BASISDIM,BLKSZ,CNVRTX,IK,IR,ITNO,IW,IXOLD,IZ,LDA,LOCLEN,
     +        MAXIT,N,NPROCS,PRECONTYPE,PROCID,STATUS,STEPERR,STOPTYPE
*     ..
*     .. External Functions ..
      REAL SCSETRHSSTOP
      EXTERNAL SCSETRHSSTOP
*     ..
*     .. External Subroutines ..
      EXTERNAL CAXPY,CCOPY,CSCAL,CSWAP,PIMSGETPAR,STOPCRIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC CMPLX,MAX
*     ..
      CALL PIMSGETPAR(IPAR,SPAR,LDA,N,BLKSZ,LOCLEN,BASISDIM,NPROCS,
     +                PROCID,PRECONTYPE,STOPTYPE,MAXIT,ITNO,STATUS,
     +                STEPERR,EPSILON,EXITNORM)

*  Check consistency of stop types
      IF ((STOPTYPE.NE.1) .AND. (STOPTYPE.NE.2) .AND.
     +    (STOPTYPE.NE.7)) THEN
          ITNO = 0
          STATUS = -6
          STEPERR = 0
          GO TO 9999

      END IF

*  Does not need conversion Y=Q2X for residual
      CNVRTX = 0

*  Set indices for mapping local vectors into wrk
      IW = 1
      IK = IW + LOCLEN
      IZ = IK + LOCLEN
      IR = IZ + LOCLEN
      IXOLD = IR + LOCLEN

*  Set rhs of stopping criteria
      RHSSTOP = SCSETRHSSTOP(B,WRK(IR),EPSILON,IPAR,PRECONL,PSCNRM)

*  1. Set parameters for iteration
      IF ((SPAR(3).EQ.ZERO) .AND. (SPAR(4).EQ.ZERO) .AND.
     +    (SPAR(5).EQ.ZERO) .AND. (SPAR(6).EQ.ZERO)) THEN
          STATUS = -7
          STEPERR = 1
          GO TO 9999

      ELSE IF (SPAR(5).EQ.SPAR(6)) THEN
*     Eigenvalues are contained in the interval [SPAR(3),SPAR(4)] on
*     the real axis:
*         sigma=(dpar(4)-dpar(3))/(2-dpar(4)-dpar(3))
*         gamma=2/(2-dpar(4)-dpar(3))
          SIGMA = (SPAR(4)-SPAR(3))/ (TWO-SPAR(4)-SPAR(3))
          SIGMASQ = SIGMA*SIGMA
          GAMMA = TWO/ (TWO-SPAR(4)-SPAR(3))

      ELSE IF (SPAR(3).EQ.SPAR(4)) THEN
*     Eigenvalues are contained in the interval [SPAR(5),SPAR(6)] on
*     the imaginary axis:
*         sigma^2=-max(dpar(5),dpar(6))
*         gamma=1
          SIGMASQ = -MAX(SPAR(5),SPAR(6))
          GAMMA = ONE

      ELSE
*     Eigenvalues are complex and contained in the box
*     SPAR(3)<= Real(e) <= SPAR(4) and SPAR(5)<= Imag(e) <= SPAR(6).
*     Compute the minimum bounding ellipse that circumscribes the box;
*     this is defined by its axes a=sqrt(2)*(dpar(4)-dpar(3))/2 (along
*     the real axis) and b=sqrt(2)*(dpar(6)-dpar(5))/2 (along the
*     imaginary axis). The center of the ellipse is d.
*         sigma^2=(a^2+b^2)/(1-d)^2
*         gamma=1/(1-d)
          LENGTHR = (SPAR(4)-SPAR(3))/TWO
          LENGTHI = (SPAR(6)-SPAR(5))/TWO
          AXISRSQ = LENGTHR*LENGTHR*TWO
          AXISISQ = LENGTHI*LENGTHI*TWO
          D = (SPAR(6)+SPAR(5))/TWO
          SIGMASQ = (AXISRSQ-AXISISQ)/ (ONE-D)**2
          GAMMA = ONE/ (ONE-D)

      END IF

*  2. k=gamma*Q1b
      IF (PRECONTYPE.EQ.0) THEN
          CALL CCOPY(LOCLEN,B,1,WRK(IK),1)
          CALL CSCAL(LOCLEN,CMPLX(GAMMA),WRK(IK),1)

      ELSE IF ((PRECONTYPE.EQ.1) .OR. (PRECONTYPE.EQ.3)) THEN
          CALL PRECONL(B,WRK(IK),IPAR)
          CALL CSCAL(LOCLEN,CMPLX(GAMMA),WRK(IK),1)
      END IF

*    xold=x
      CALL CCOPY(LOCLEN,X,1,WRK(IXOLD),1)

*  Loop
      STATUS = 0
      EXITNORM = -ONE
      STEPERR = -1
      DO 10 ITNO = 1,MAXIT

*  3. rho
          IF (ITNO.EQ.1) THEN
              RHO = ONE

          ELSE IF (ITNO.EQ.2) THEN
              RHO = ONE/ (ONE-SIGMASQ/TWO)

          ELSE
              RHO = ONE/ (ONE-RHO*SIGMASQ/4.0)
          END IF

*  4. w=(I-Q1AQ2)x
          IF (PRECONTYPE.EQ.0) THEN
              CALL MATVEC(X,WRK(IZ),IPAR)

          ELSE IF (PRECONTYPE.EQ.1) THEN
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)

          ELSE IF (PRECONTYPE.EQ.2) THEN
              CALL PRECONR(X,WRK(IW),IPAR)
              CALL MATVEC(WRK(IW),WRK(IZ),IPAR)

          ELSE IF (PRECONTYPE.EQ.3) THEN
              CALL PRECONR(X,WRK(IZ),IPAR)
              CALL MATVEC(WRK(IZ),WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)
          END IF

          CALL CCOPY(LOCLEN,X,1,WRK(IW),1)
          CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IW),1)

*  5. x=rho*(gamma*((I-Q1A)x+Q1b)+(1-gamma)*x)+(1-rho)*xold
          DELTA = RHO*GAMMA
          CALL CSCAL(LOCLEN,CMPLX(ONE-RHO),WRK(IXOLD),1)
          CALL CAXPY(LOCLEN,CMPLX(RHO),WRK(IK),1,WRK(IXOLD),1)
          CALL CAXPY(LOCLEN,CMPLX(RHO-DELTA),X,1,WRK(IXOLD),1)
          CALL CAXPY(LOCLEN,CMPLX(DELTA),WRK(IW),1,WRK(IXOLD),1)
          CALL CSWAP(LOCLEN,WRK(IXOLD),1,X,1)

*  6. check stopping criterion
          CALL STOPCRIT(B,WRK(IZ),WRK(IR),X,WRK(IXOLD),WRK(IW),RHSSTOP,
     +                  CNVRTX,EXITNORM,STATUS,IPAR,MATVEC,MATVEC,
     +                  PRECONR,PCSUM,PSCNRM)

*  Call monitoring routine
          CALL PROGRESS(LOCLEN,ITNO,EXITNORM,X,WRK(IR),WRK(IR))

          IF (STATUS.EQ.0) THEN
              GO TO 9999
          END IF

   10 CONTINUE

      IF (ITNO.GT.MAXIT) THEN
          STATUS = -1
          ITNO = MAXIT
      END IF

 9999 CONTINUE

      IF ((PRECONTYPE.EQ.2) .OR. (PRECONTYPE.EQ.3)) THEN
          CALL CCOPY(LOCLEN,X,1,WRK(IZ),1)
          CALL PRECONR(WRK(IZ),X,IPAR)
      END IF

*  Set output parameters
      IPAR(11) = ITNO
      IPAR(12) = STATUS
      IPAR(13) = STEPERR
      SPAR(2) = EXITNORM

      RETURN

      END
