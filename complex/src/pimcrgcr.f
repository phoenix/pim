      SUBROUTINE PIMCRGCR(X,B,WRK,IPAR,SPAR,MATVEC,PRECONL,PRECONR,
     +                    PCSUM,PSCNRM,PROGRESS)
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      REAL ONE
      PARAMETER (ONE=1.0E0)
      COMPLEX CZERO
      PARAMETER (CZERO= (0.0E0,0.0E0))
      COMPLEX CONE
      PARAMETER (CONE= (1.0E0,0.0E0))
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=2)
*     ..
*     .. Array Arguments ..
      COMPLEX B(*),WRK(*),X(*)
      REAL SPAR(SPARSIZ)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Function Arguments ..
      REAL PSCNRM
      EXTERNAL PSCNRM
*     ..
*     .. Subroutine Arguments ..
      EXTERNAL MATVEC,PCSUM,PRECONL,PRECONR,PROGRESS
*     ..
*     .. Local Scalars ..
      COMPLEX ALPHA,BETA,XI
      REAL EPSILON,EXITNORM,RHSSTOP
      INTEGER BASISDIM,BLKSZ,CNVRTX,I,IDOTS,IP,IQ,IR,ITNO,IW,IXOLD,IZ,
     +        IZETA,J,J0,K,K1,LDA,LOCLEN,MAXIT,N,NPROCS,PRECONTYPE,
     +        PROCID,STATUS,STEPERR,STOPTYPE
*     ..
*     .. Local Arrays ..
      COMPLEX DOTS(2)
*     ..
*     .. External Functions ..
      COMPLEX CDOTC
      REAL SCSETRHSSTOP
      EXTERNAL CDOTC,SCSETRHSSTOP
*     ..
*     .. External Subroutines ..
      EXTERNAL CAXPY,CCOPY,CINIT,PIMSGETPAR,STOPCRIT
*     ..

      CALL PIMSGETPAR(IPAR,SPAR,LDA,N,BLKSZ,LOCLEN,BASISDIM,NPROCS,
     +                PROCID,PRECONTYPE,STOPTYPE,MAXIT,ITNO,STATUS,
     +                STEPERR,EPSILON,EXITNORM)

*  Check consistency of preconditioning and stop types
      IF (((PRECONTYPE.EQ.0).OR. (PRECONTYPE.EQ.2)) .AND.
     +    (STOPTYPE.EQ.6)) THEN
          ITNO = 0
          STATUS = -4
          STEPERR = 0
          GO TO 9999

      END IF

*  Does not need conversion Y=Q2X for residual
      CNVRTX = 0

*  Set indices for mapping local vectors into wrk
      IR = 1
      IP = IR + LOCLEN
      IW = IP + BASISDIM*LOCLEN
      IZETA = IW + BASISDIM*LOCLEN
      IZ = IZETA + BASISDIM
      IQ = IZ + LOCLEN
      IXOLD = IQ + LOCLEN
      IDOTS = IXOLD + LOCLEN

*  Set rhs of stopping criteria
      RHSSTOP = SCSETRHSSTOP(B,WRK(IR),EPSILON,IPAR,PRECONL,PSCNRM)

*  1. r=Q1(b-AQ2x)
      IF (STOPTYPE.NE.6) THEN
          IF (PRECONTYPE.EQ.0) THEN
*     r=b-Ax
              CALL CCOPY(LOCLEN,B,1,WRK(IR),1)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IW),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.1) THEN
*     r=Q1(b-Ax)
              CALL CCOPY(LOCLEN,B,1,WRK(IZ),1)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IW),1,WRK(IZ),1)
              CALL PRECONL(WRK(IZ),WRK(IR),IPAR)

          ELSE IF (PRECONTYPE.EQ.2) THEN
*     r=b-AQ2x
              CALL CCOPY(LOCLEN,B,1,WRK(IR),1)
              CALL PRECONR(X,WRK(IW),IPAR)
              CALL MATVEC(WRK(IW),WRK(IZ),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.3) THEN
*     r=Q1(b-AQ2x)
              CALL CCOPY(LOCLEN,B,1,WRK(IP),1)
              CALL PRECONR(X,WRK(IW),IPAR)
              CALL MATVEC(WRK(IW),WRK(IZ),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IP),1)
              CALL PRECONL(WRK(IP),WRK(IR),IPAR)
          END IF

      ELSE
*     r has been set to Qb in the call to dsetrhsstop
          IF (PRECONTYPE.EQ.1) THEN
*     r=Q1(b-Ax)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.3) THEN
*     r=Q1(b-AQ2x)
              CALL PRECONR(X,WRK(IZ),IPAR)
              CALL MATVEC(WRK(IZ),WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)
              CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IR),1)
          END IF

      END IF

*  Loop
      STATUS = 0
      EXITNORM = -ONE
      STEPERR = -1
      DO 20 ITNO = 1,MAXIT

*  2. p(1)=r
          CALL CCOPY(LOCLEN,WRK(IR),1,WRK(IP),1)

          K = 0
          DO 40 J = 1,BASISDIM
              J0 = J - 1

*  3. w(j)=Q1AQ2p(j)
              IF (PRECONTYPE.EQ.0) THEN
                  CALL MATVEC(WRK(IP+K),WRK(IW+K),IPAR)

              ELSE IF (PRECONTYPE.EQ.1) THEN
                  CALL MATVEC(WRK(IP+K),WRK(IZ),IPAR)
                  CALL PRECONL(WRK(IZ),WRK(IW+K),IPAR)

              ELSE IF (PRECONTYPE.EQ.2) THEN
                  CALL PRECONR(WRK(IP+K),WRK(IZ),IPAR)
                  CALL MATVEC(WRK(IZ),WRK(IW+K),IPAR)

              ELSE IF (PRECONTYPE.EQ.3) THEN
                  CALL PRECONR(WRK(IP+K),WRK(IW+K),IPAR)
                  CALL MATVEC(WRK(IW+K),WRK(IZ),IPAR)
                  CALL PRECONL(WRK(IZ),WRK(IW+K),IPAR)
              END IF

*  4. zeta(j)=dot(w(j),w(j))
              DOTS(1) = CDOTC(LOCLEN,WRK(IW+K),1,WRK(IW+K),1)

*  5. xi=dot(r,w(j))
              DOTS(2) = CDOTC(LOCLEN,WRK(IR),1,WRK(IW+K),1)

*  Accumulate simultaneously partial values
              CALL PCSUM(2,DOTS,IPAR)
              WRK(IZETA+J0) = DOTS(1)
              XI = DOTS(2)

*  5. alpha=xi/zeta(j)
              IF (WRK(IZETA+J0).EQ.CZERO) THEN
                  STATUS = -3
                  STEPERR = 5
                  GO TO 9999

              END IF

              ALPHA = XI/WRK(IZETA+J0)

*  6. x=x+alpha*p(j)
              CALL CCOPY(LOCLEN,X,1,WRK(IXOLD),1)
              CALL CAXPY(LOCLEN,ALPHA,WRK(IP+K),1,X,1)

*  7. r=r-alpha*w(j)
              CALL CAXPY(LOCLEN,-ALPHA,WRK(IW+K),1,WRK(IR),1)

*  8. check stopping criterion
              CALL STOPCRIT(B,WRK(IR),WRK(IZ),X,WRK(IXOLD),WRK(IQ),
     +                      RHSSTOP,CNVRTX,EXITNORM,STATUS,IPAR,MATVEC,
     +                      MATVEC,PRECONR,PCSUM,PSCNRM)

*  Call monitoring routine
              CALL PROGRESS(LOCLEN,ITNO,EXITNORM,X,WRK(IR),WRK(IZ))

              IF (STATUS.EQ.0) THEN
                  GO TO 9999
              END IF

*  9. q=Q1AQ2r
              IF (PRECONTYPE.EQ.0) THEN
                  CALL MATVEC(WRK(IR),WRK(IQ),IPAR)

              ELSE IF (PRECONTYPE.EQ.1) THEN
                  CALL MATVEC(WRK(IR),WRK(IZ),IPAR)
                  CALL PRECONL(WRK(IZ),WRK(IQ),IPAR)

              ELSE IF (PRECONTYPE.EQ.2) THEN
                  CALL PRECONR(WRK(IR),WRK(IZ),IPAR)
                  CALL MATVEC(WRK(IZ),WRK(IQ),IPAR)

              ELSE IF (PRECONTYPE.EQ.3) THEN
                  CALL PRECONR(WRK(IR),WRK(IQ),IPAR)
                  CALL MATVEC(WRK(IQ),WRK(IZ),IPAR)
                  CALL PRECONL(WRK(IZ),WRK(IQ),IPAR)
              END IF

* 10. p(j+1)=r-sum_{i=1}^{j}{dot(q,w(i))/zeta(i)*p(i)}
              IF (J.LT.BASISDIM) THEN

*  Compute partial inner-products
                  K1 = 0
                  DO 50 I = 0,J - 1
                      WRK(IDOTS+I) = CDOTC(LOCLEN,WRK(IQ),1,WRK(IW+K1),
     +                               1)
                      K1 = K1 + LOCLEN
   50             CONTINUE

*  Accumulate simultaneously partial values
                  CALL PCSUM(J,WRK(IDOTS),IPAR)

*  Compute summation
                  CALL CINIT(LOCLEN,CZERO,WRK(IZ),1)
                  K1 = 0
                  DO 60 I = 0,J - 1
                      BETA = WRK(IDOTS+I)/WRK(IZETA+I)
                      CALL CAXPY(LOCLEN,BETA,WRK(IP+K1),1,WRK(IZ),1)
                      K1 = K1 + LOCLEN
   60             CONTINUE

*  Compute p(j+1)
                  K = K + LOCLEN
                  CALL CCOPY(LOCLEN,WRK(IR),1,WRK(IP+K),1)
                  CALL CAXPY(LOCLEN,-CONE,WRK(IZ),1,WRK(IP+K),1)
              END IF

   40     CONTINUE

   20 CONTINUE

      IF (ITNO.GT.MAXIT) THEN
          STATUS = -1
          ITNO = MAXIT
      END IF

 9999 CONTINUE

      IF ((PRECONTYPE.EQ.2) .OR. (PRECONTYPE.EQ.3)) THEN
          CALL CCOPY(LOCLEN,X,1,WRK(IZ),1)
          CALL PRECONR(WRK(IZ),X,IPAR)
      END IF

*  Set output parameters
      IPAR(11) = ITNO
      IPAR(12) = STATUS
      IPAR(13) = STEPERR
      SPAR(2) = EXITNORM

      RETURN

      END
