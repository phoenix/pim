      PROGRAM MACHCONS
      IMPLICIT NONE
*
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     To obtain the desired machine-dependent floating-point constants;
*  link with LAPACK library (needs DLAMCH and related subroutines).
*     This program is based on LAPACK testing programmes.

*     .. Local Scalars ..
      DOUBLE PRECISION EPS,RMAX,RMIN
*     ..
*     .. External Functions ..
      DOUBLE PRECISION DLAMCH
      EXTERNAL DLAMCH
*     ..
      EPS = DLAMCH('E')
      RMIN = DLAMCH('U')
      RMAX = DLAMCH('O')

      WRITE(6,*)'sed s/MACHEPSVAL/"',EPS,'"/g dmachcons.f.orig > aaa'
      WRITE(6,*)'sed s/UNDERFLOWVAL/"',RMIN,'"/g aaa > bbb'
      WRITE(6,*)'sed s/OVERFLOWVAL/"',RMAX,'"/g bbb > ccc'
      WRITE(6,*)'awk -f fixexp.awk ccc > dmachcons.f'
      WRITE(6,*)'rm -f aaa bbb ccc'
 
      STOP
      END
