      SUBROUTINE SMACHCONS(WHAT,RESULT)

* These values are for IEEE-754 arithmetic

*     .. Parameters ..
      REAL MACHEPS
      PARAMETER (MACHEPS=    5.96046E-08)
      REAL UNDERFLOW
      PARAMETER (UNDERFLOW=    1.17549E-38)
      REAL OVERFLOW
      PARAMETER (OVERFLOW=    3.40282E+38)
*     ..
*     .. Scalar Arguments ..
      REAL RESULT
      CHARACTER WHAT
*     ..
      IF ((WHAT.EQ.'M') .OR. (WHAT.EQ.'m')) THEN
          RESULT = MACHEPS
      ELSE IF ((WHAT.EQ.'U') .OR. (WHAT.EQ.'u')) THEN
          RESULT = UNDERFLOW
      ELSE IF ((WHAT.EQ.'O') .OR. (WHAT.EQ.'o')) THEN
          RESULT = OVERFLOW
      END IF

      RETURN

      END
