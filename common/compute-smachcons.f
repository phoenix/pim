      PROGRAM MACHCONS
      IMPLICIT NONE
*
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     To obtain the desired machine-dependent floating-point constants;
*  link with LAPACK library (needs DLAMCH and related subroutines).
*     This program is based on LAPACK testing programmes.

*     ..
*     .. Local Scalars ..
      REAL EPS,RMAX,RMIN
*     ..
*     .. External Functions ..
      REAL SLAMCH
      EXTERNAL SLAMCH
*     ..
      EPS = SLAMCH('E')
      RMIN = SLAMCH('U')
      RMAX = SLAMCH('O')

      WRITE(6,*)'sed s/MACHEPSVAL/"',EPS,'"/g smachcons.f.orig > aaa '
      WRITE(6,*)'sed s/UNDERFLOWVAL/"',RMIN,'"/g aaa > bbb '
      WRITE(6,*)'sed s/OVERFLOWVAL/"',RMAX,'"/g bbb > ccc'
      WRITE(6,*)'awk -f fixexp.awk ccc > smachcons.f'
      WRITE(6,*)'rm -f aaa bbb ccc'
 
      STOP
      END
