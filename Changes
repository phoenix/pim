11-January-1994:
--
  1. PIM 1.0 is released

13-January-1994:
--
  1. Update Readme file with instructions for building a minimal BLAS
     library containing the BLAS routines needed by PIM 1.0

15-January-1994:
--
  1. Update Readme file correcting location of some files

17-January-1994:
--
  1. Fixed the matrix-vector (and matrix-transpose-vector) product routines 
     of the PDE examples for the Intel Paragon

20-January-1994:
--
  1. Ported to SGI Indy II (IRIX 5.1.1/MIPS Fortran 77 5.0)

25-January-1994:
--
  1. Fixed examples for dense matrices (PVM, p4, TCGMSG, Paragon)

18-February-1994:
--
  1. Released version 1.1; changes include:
     a. COMPLEX and DOUBLE COMPLEX versions of the PIM routines.
     b. Removed control of divergence and stagnation (caused methods
        stopping too early)
     c. Fixed some bugs in the examples
     d. Updated user's guide

1st-July-1995:
--
  1. Released version 2.0; changes include:
     a. Included four new implementations of iterative methods:
        - Restarted, stabilized Bi-CGSTAB (RBi-CGSTAB) [Sleijpen and Fokkema,
          1993]
        - GMRES with eigenvalues estimation (RGMRESEV)
        - QMR with coupled two-term recurrences [Freund, 1992]
        - Chebyshev acceleration
     b. Included monitoring routine to the iterative methods routines
     c. Changed interface of the iterative methods routines
     d. Provided program to compute machine-dependent floating-point
        constants
     e. Included example programs using MPI; dropped examples using p4, 
        NXLIB and TCGMSG
     f. Fixed some bugs in the examples
     g. Updated user's guide

28th-June-1995:
--
  1. Released version 2.1; changes include:
     a. Replaced previous implementation of the QMR (coupled two-term
	recurrences by Freund) by the new, highly parallel algorithm
	by Bucker and Sauren
     b.	All PDE examples have been updated to allow the use of rectangular
	finite-differences grids
     c. Fixed some bugs in the COMPLEX/DCOMPLEX iterative routines
     d. Updated user's guide

12th-May-1997:
--
  1. Released version 2.2; change on the type/number of arguments passed
     to the PDSUM/PDNRM functions.

20th-June-2003:
--
  1. Released version 2.3; fixed minor bug on PIMxRGMRESEV calls to xHSEQR

10th-May-2023:
--
  1. Changed license to GPLv3, moved source to https://gitlab.mpcdf.mpg.de/phoenix/pim
