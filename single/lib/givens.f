
      SUBROUTINE GIVENS(A,B,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      REAL A,B,C,S
*     ..
*     .. Local Scalars ..
      REAL TAU
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      REAL ZERO
      PARAMETER (ZERO=0.0)
      REAL ONE
      PARAMETER (ONE=1.0)
*     ..
      IF (B.EQ.ZERO) THEN
          C = ONE
          S = ZERO

      ELSE IF (ABS(B).GT.ABS(A)) THEN
          TAU = -A/B
          S = ONE/SQRT(ONE+TAU**2)
          C = S*TAU

      ELSE
          TAU = -B/A
          C = ONE/SQRT(ONE+TAU**2)
          S = C*TAU
      END IF

      RETURN

      END
