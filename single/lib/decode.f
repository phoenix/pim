
      SUBROUTINE DECODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      REAL C,RHO,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      REAL ZERO
      PARAMETER (ZERO=0.0)
      REAL ONE
      PARAMETER (ONE=1.0)
*     ..
      IF (RHO.EQ.ONE) THEN
          C = ZERO
          S = ONE

      ELSE IF (ABS(RHO).LT.ONE) THEN
          S = 2.0*RHO
          C = SQRT(ONE-S**2)

      ELSE
          C = 2.0/RHO
          S = SQRT(ONE-C**2)
      END IF

      RETURN

      END
