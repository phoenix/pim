
      SUBROUTINE ENCODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      REAL C,RHO,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SIGN
*     ..
*     .. Parameters ..
      REAL ZERO
      PARAMETER (ZERO=0.0)
      REAL ONE
      PARAMETER (ONE=1.0)
*     ..
      IF (C.EQ.ZERO) THEN
          RHO = ONE

      ELSE IF (ABS(S).LT.ABS(C)) THEN
          RHO = SIGN(ONE,C)*S/2.0

      ELSE
          RHO = 2.0*SIGN(ONE,S)/C
      END IF

      RETURN

      END
