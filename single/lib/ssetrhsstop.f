      REAL FUNCTION SSETRHSSTOP(B,WRK,EPSILON,IPAR,PRECONL,PSNRM)
      IMPLICIT NONE

*     .. Scalar Arguments ..
      REAL EPSILON
*     ..
*     .. Array Arguments ..
      REAL B(*),WRK(*)
      INTEGER IPAR(*)
*     ..
*     .. Function Arguments ..
      REAL PSNRM
      EXTERNAL PSNRM
*     ..
*     .. Subroutine Arguments ..
      EXTERNAL PRECONL
*     ..
*     .. Local Scalars ..
      INTEGER LOCLEN,STOPTYPE
*     ..
      LOCLEN = IPAR(4)
      STOPTYPE = IPAR(9)
      IF ((STOPTYPE.EQ.1) .OR. (STOPTYPE.EQ.4) .OR.
     +    (STOPTYPE.EQ.7)) THEN
*  ||r||<epsilon or ||Q1r||<epsilon ||x(k)-x(k-1)||<epsilon
          SSETRHSSTOP = EPSILON

      ELSE IF ((STOPTYPE.EQ.2) .OR. (STOPTYPE.EQ.3) .OR.
     +         (STOPTYPE.EQ.5)) THEN
*  ||r||<epsilon||b|| or sqrt(r(Q1r))<epsilon||b|| or
*  ||Q1r||<epsilon||b||
          SSETRHSSTOP = EPSILON*PSNRM(LOCLEN,B,IPAR)

      ELSE IF (STOPTYPE.EQ.6) THEN
*  ||Q1r||<epsilon||Q1b||
          CALL PRECONL(B,WRK,IPAR)
          SSETRHSSTOP = EPSILON*PSNRM(LOCLEN,WRK,IPAR)
      END IF

      RETURN

      END
