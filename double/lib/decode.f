
      SUBROUTINE DECODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION C,RHO,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
      DOUBLE PRECISION ONE
      PARAMETER (ONE=1.0D0)
      DOUBLE PRECISION TWO
      PARAMETER (TWO=2.0D0)
*     ..
      IF (RHO.EQ.ONE) THEN
          C = ZERO
          S = ONE

      ELSE IF (ABS(RHO).LT.ONE) THEN
          S = TWO*RHO
          C = SQRT(ONE-S**2)

      ELSE
          C = TWO/RHO
          S = SQRT(ONE-C**2)
      END IF

      RETURN

      END
