      DOUBLE PRECISION FUNCTION DSETRHSSTOP(B,WRK,EPSILON,IPAR,PRECONL,
     +                 PDNRM)
      IMPLICIT NONE

*     .. Scalar Arguments ..
      DOUBLE PRECISION EPSILON
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION B(*),WRK(*)
      INTEGER IPAR(*)
*     ..
*     .. Function Arguments ..
      DOUBLE PRECISION PDNRM
      EXTERNAL PDNRM
*     ..
*     .. Subroutine Arguments ..
      EXTERNAL PRECONL
*     ..
*     .. Local Scalars ..
      INTEGER LOCLEN,STOPTYPE
*     ..
      LOCLEN = IPAR(4)
      STOPTYPE = IPAR(9)
      IF ((STOPTYPE.EQ.1) .OR. (STOPTYPE.EQ.4) .OR.
     +    (STOPTYPE.EQ.7)) THEN
*  ||r||<epsilon or ||Q1r||<epsilon ||x(k)-x(k-1)||<epsilon
          DSETRHSSTOP = EPSILON

      ELSE IF ((STOPTYPE.EQ.2) .OR. (STOPTYPE.EQ.3) .OR.
     +         (STOPTYPE.EQ.5)) THEN
*  ||r||<epsilon||b|| or sqrt(r(Q1r))<epsilon||b|| or
*  ||Q1r||<epsilon||b||
          DSETRHSSTOP = EPSILON*PDNRM(LOCLEN,B,IPAR)

      ELSE IF (STOPTYPE.EQ.6) THEN
*  ||Q1r||<epsilon||Q1b||
          CALL PRECONL(B,WRK,IPAR)
          DSETRHSSTOP = EPSILON*PDNRM(LOCLEN,WRK,IPAR)
      END IF

      RETURN

      END
