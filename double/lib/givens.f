
      SUBROUTINE GIVENS(A,B,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION A,B,C,S
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TAU
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
      DOUBLE PRECISION ONE
      PARAMETER (ONE=1.0D0)
*     ..
      IF (B.EQ.ZERO) THEN
          C = ONE
          S = ZERO

      ELSE IF (ABS(B).GT.ABS(A)) THEN
          TAU = -A/B
          S = ONE/SQRT(ONE+TAU**2)
          C = S*TAU

      ELSE
          TAU = -B/A
          C = ONE/SQRT(ONE+TAU**2)
          S = C*TAU
      END IF

      RETURN

      END
