
      SUBROUTINE ENCODE(RHO,C,S)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION C,RHO,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SIGN
*     ..
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
      DOUBLE PRECISION ONE
      PARAMETER (ONE=1.0D0)
      DOUBLE PRECISION TWO
      PARAMETER (TWO=2.0D0)
*     ..
      IF (C.EQ.ZERO) THEN
          RHO = ONE

      ELSE IF (ABS(S).LT.ABS(C)) THEN
          RHO = SIGN(ONE,C)*S/TWO

      ELSE
          RHO = TWO*SIGN(ONE,S)/C
      END IF

      RETURN

      END
