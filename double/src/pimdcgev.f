      SUBROUTINE PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,PRECONL,PRECONR,
     +                    PDSUM,PDNRM,PROGRESS)
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
      DOUBLE PRECISION ONE
      PARAMETER (ONE=1.0D0)
      DOUBLE PRECISION TWO
      PARAMETER (TWO=2.0D0)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION B(*),DPAR(DPARSIZ),WRK(*),X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Function Arguments ..
      DOUBLE PRECISION PDNRM
      EXTERNAL PDNRM
*     ..
*     .. Subroutine Arguments ..
      EXTERNAL MATVEC,PDSUM,PRECONL,PRECONR,PROGRESS
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION A1,ALPHA,B1,BETA,C,D,D1,DELTA,E,EPSILON,EXITNORM,
     +                 F,F0,F1,G,H,H0,H1,MACHEPS,MU1,MUN,RDOTR,RDOTR0,
     +                 RHO,RHO0,RHSSTOP,TA,TA1,TB,TB0,XI,XI0
      INTEGER BASISDIM,BLKSZ,CNVRTX,IDT,IET,IP,IR,IRSLT,IS,ITNO,IW,
     +        IXOLD,IZ,LDA,LOCLEN,MAXIT,N,NPROCS,PRECONTYPE,PROCID,
     +        STATUS,STEPERR,STOPTYPE
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION DOTS(2)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION BISECTION,DDOT,DSETRHSSTOP
      EXTERNAL BISECTION,DDOT,DSETRHSSTOP
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DMACHCONS,PIMDGETPAR,STOPCRIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,MAX,MIN,SQRT
*     ..

*     .. Scalars in Common ..
      DOUBLE PRECISION OLIMIT,OVERFLOW,ULIMIT,UNDERFLOW
*     ..
*     .. Common blocks ..
      COMMON /MACHCONS1/UNDERFLOW,OVERFLOW
      COMMON /MACHCONS2/ULIMIT,OLIMIT
*     ..
      CALL DMACHCONS('M',MACHEPS)
      CALL DMACHCONS('U',UNDERFLOW)
      ULIMIT = SQRT(UNDERFLOW)*TWO
      CALL DMACHCONS('O',OVERFLOW)
      OLIMIT = SQRT(OVERFLOW)/TWO

      CALL PIMDGETPAR(IPAR,DPAR,LDA,N,BLKSZ,LOCLEN,BASISDIM,NPROCS,
     +                PROCID,PRECONTYPE,STOPTYPE,MAXIT,ITNO,STATUS,
     +                STEPERR,EPSILON,EXITNORM)

*  Check consistency of preconditioning and stop types
      IF (((PRECONTYPE.EQ.0).OR. (PRECONTYPE.EQ.2)) .AND.
     +    (STOPTYPE.EQ.6)) THEN
          ITNO = 0
          STATUS = -4
          STEPERR = 0
          GO TO 9999

      END IF

*  Does not need conversion Y=Q2X for residual
      CNVRTX = 0

*  Set indices for mapping local vectors into wrk
      IR = 1
      IP = IR + LOCLEN
      IW = IP + LOCLEN
      IZ = IW + LOCLEN
      IS = IZ + LOCLEN
      IXOLD = IS + LOCLEN
      IDT = IXOLD + LOCLEN
      IET = IDT + MAXIT + 1

*  Set rhs of stopping criteria
      RHSSTOP = DSETRHSSTOP(B,WRK(IR),EPSILON,IPAR,PRECONL,PDNRM)

*  1. r=Q1(b-AQ2x)
      IF (STOPTYPE.NE.6) THEN
          IF (PRECONTYPE.EQ.0) THEN
*     r=b-Ax
              CALL DCOPY(LOCLEN,B,1,WRK(IR),1)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IW),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.1) THEN
*     r=Q1(b-Ax)
              CALL DCOPY(LOCLEN,B,1,WRK(IZ),1)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IW),1,WRK(IZ),1)
              CALL PRECONL(WRK(IZ),WRK(IR),IPAR)

          ELSE IF (PRECONTYPE.EQ.2) THEN
*     r=b-AQ2x
              CALL DCOPY(LOCLEN,B,1,WRK(IR),1)
              CALL PRECONR(X,WRK(IW),IPAR)
              CALL MATVEC(WRK(IW),WRK(IZ),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IZ),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.3) THEN
*     r=Q1(b-AQ2x)
              CALL DCOPY(LOCLEN,B,1,WRK(IP),1)
              CALL PRECONR(X,WRK(IW),IPAR)
              CALL MATVEC(WRK(IW),WRK(IZ),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IZ),1,WRK(IP),1)
              CALL PRECONL(WRK(IP),WRK(IR),IPAR)
          END IF

      ELSE
*     r has been set to Qb in the call to dsetrhsstop
          IF (PRECONTYPE.EQ.1) THEN
*     r=Q1(b-Ax)
              CALL MATVEC(X,WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IZ),1,WRK(IR),1)

          ELSE IF (PRECONTYPE.EQ.3) THEN
*     r=Q1(b-AQ2x)
              CALL PRECONR(X,WRK(IZ),IPAR)
              CALL MATVEC(WRK(IZ),WRK(IW),IPAR)
              CALL PRECONL(WRK(IW),WRK(IZ),IPAR)
              CALL DAXPY(LOCLEN,-ONE,WRK(IZ),1,WRK(IR),1)
          END IF

      END IF

*  2. p=r
      CALL DCOPY(LOCLEN,WRK(IR),1,WRK(IP),1)

*  3. rdotr=dot(r,r)
      DOTS(1) = DDOT(LOCLEN,WRK(IR),1,WRK(IR),1)
      CALL PDSUM(1,DOTS,IPAR)
      RDOTR = DOTS(1)

*  4. w=Q1AQ2p
      IF (PRECONTYPE.EQ.0) THEN
          CALL MATVEC(WRK(IP),WRK(IW),IPAR)

      ELSE IF (PRECONTYPE.EQ.1) THEN
          CALL MATVEC(WRK(IP),WRK(IZ),IPAR)
          CALL PRECONL(WRK(IZ),WRK(IW),IPAR)

      ELSE IF (PRECONTYPE.EQ.2) THEN
          CALL PRECONR(WRK(IP),WRK(IZ),IPAR)
          CALL MATVEC(WRK(IZ),WRK(IW),IPAR)

      ELSE IF (PRECONTYPE.EQ.3) THEN
          CALL PRECONR(WRK(IP),WRK(IW),IPAR)
          CALL MATVEC(WRK(IW),WRK(IZ),IPAR)
          CALL PRECONL(WRK(IZ),WRK(IW),IPAR)
      END IF

* 5. xi=dot(p,w)
      DOTS(1) = DDOT(LOCLEN,WRK(IP),1,WRK(IW),1)
      CALL PDSUM(1,DOTS,IPAR)
      XI = DOTS(1)

* 6. Initialise data for eigenvalue estimation
      MU1 = ZERO
      MUN = ZERO
      RHO = SQRT(RDOTR)
*  Use rdotr==rho**2
      IF (RDOTR.EQ.ZERO) THEN
          ITNO = 0
          STATUS = -3
          STEPERR = 6
          GO TO 9999

      END IF

      TA1 = XI/RDOTR
      WRK(IDT) = TA1
      TB = ZERO

*  Loop
      STATUS = 0
      EXITNORM = -ONE
      STEPERR = -1
      DO 20 ITNO = 1,MAXIT

*  7. alpha=rdotr/xi
          IF (XI.EQ.ZERO) THEN
              STATUS = -3
              STEPERR = 7
              GO TO 9999

          END IF

          ALPHA = RDOTR/XI

*  8. x=x+alpha*p
          CALL DCOPY(LOCLEN,X,1,WRK(IXOLD),1)
          CALL DAXPY(LOCLEN,ALPHA,WRK(IP),1,X,1)

*  9. r=r-alpha*w
          CALL DAXPY(LOCLEN,-ALPHA,WRK(IW),1,WRK(IR),1)

* 10. check stopping criterion
          CALL STOPCRIT(B,WRK(IR),WRK(IZ),X,WRK(IXOLD),WRK(IS),RHSSTOP,
     +                  CNVRTX,EXITNORM,STATUS,IPAR,MATVEC,MATVEC,
     +                  PRECONR,PDSUM,PDNRM)

*  Call monitoring routine
          CALL PROGRESS(LOCLEN,ITNO,EXITNORM,X,WRK(IR),WRK(IZ))

          IF (STATUS.EQ.-5) THEN
              STEPERR = 10
              GO TO 9999
          ELSE IF (STATUS.EQ.0) THEN
              GO TO 9999
          END IF
* 11. s=Q1AQ2r
          IF (PRECONTYPE.EQ.0) THEN
              CALL MATVEC(WRK(IR),WRK(IS),IPAR)

          ELSE IF (PRECONTYPE.EQ.1) THEN
              CALL MATVEC(WRK(IR),WRK(IZ),IPAR)
              CALL PRECONL(WRK(IZ),WRK(IS),IPAR)

          ELSE IF (PRECONTYPE.EQ.2) THEN
              CALL PRECONR(WRK(IR),WRK(IZ),IPAR)
              CALL MATVEC(WRK(IZ),WRK(IS),IPAR)

          ELSE IF (PRECONTYPE.EQ.3) THEN
              CALL PRECONR(WRK(IR),WRK(IS),IPAR)
              CALL MATVEC(WRK(IS),WRK(IZ),IPAR)
              CALL PRECONL(WRK(IZ),WRK(IS),IPAR)
          END IF

* 12. rdotr=dot(r,r)
          RDOTR0 = RDOTR
          DOTS(1) = DDOT(LOCLEN,WRK(IR),1,WRK(IR),1)

* 13. delta=dot(r,s)
          DOTS(2) = DDOT(LOCLEN,WRK(IR),1,WRK(IS),1)

*  Accumulate simultaneously partial values
          CALL PDSUM(2,DOTS,IPAR)
          RDOTR = DOTS(1)
          DELTA = DOTS(2)

* 14. beta=rdotr/rdotr0
          IF (RDOTR0.EQ.ZERO) THEN
              STATUS = -3
              STEPERR = 14
              GO TO 9999

          END IF

          BETA = RDOTR/RDOTR0

* 15. p=r+beta*p
          CALL DCOPY(LOCLEN,WRK(IP),1,WRK(IZ),1)
          CALL DCOPY(LOCLEN,WRK(IR),1,WRK(IP),1)
          CALL DAXPY(LOCLEN,BETA,WRK(IZ),1,WRK(IP),1)

* 16. w=s+beta*w
          CALL DCOPY(LOCLEN,WRK(IW),1,WRK(IZ),1)
          CALL DCOPY(LOCLEN,WRK(IS),1,WRK(IW),1)
          CALL DAXPY(LOCLEN,BETA,WRK(IZ),1,WRK(IW),1)

* 17. xi=delta-beta^2*xi
          XI0 = XI
          XI = DELTA - BETA**2*XI

* 18. compute estimates of eigenvalues
          TA = TA1
*  Use rdotr==rho**2.0
          RHO0 = RHO
          RHO = SQRT(RDOTR)

          TA1 = (BETA**TWO*XI0+XI)/RDOTR
          TB0 = TB
          TB = -XI0*BETA/ (RHO0*RHO)
          WRK(IDT+ITNO) = TA

*     Store square of subdiagonal elements to save operations in
*     the bisection routine
          WRK(IET+ITNO-1) = TB*TB

          IF (ITNO.EQ.1) THEN
*     Compute roots of the funression for the determinant
*       | a1 b1 |
*       | b1 a2 |
*     i.e., a quadratic equation
*     p(mu)=mu^2-(a1+a2)mu+a1*a2-b1^2 via quadratic formula; take
*     care of possible cancellation of digits
              A1 = TA + TA1
              B1 = TA*TA1 - WRK(IET+ITNO-1)
              IF (A1.LT.ZERO) THEN
                  D1 = SQRT(A1*A1-4.0D0*B1)
                  E = (A1+D1)/TWO
                  F = TWO*B1/ (A1+D1)
                  MU1 = MIN(E,F)
                  MUN = MAX(E,F)

              ELSE IF (A1.GT.ZERO) THEN
                  D1 = SQRT(A1*A1-4.0D0*B1)
                  E = TWO*B1/ (A1-D1)
                  F = (A1-D1)/TWO
                  MU1 = MIN(E,F)
                  MUN = MAX(E,F)

              ELSE
                  D1 = SQRT(-B1)
                  MU1 = -D1
                  MUN = D1
              END IF

*     Set [c,d] to [mu1,mun]
              C = MU1
              D = MUN

          ELSE

*     Update [c,d] via Gerschgorin's theorem
              G = ABS(TB)
              E = ABS(TB0) + G
              F = ABS(TA)
              F0 = F - E
              F1 = F + E
              H = ABS(TA1)
              H0 = H - G
              H1 = H + G
              C = MIN(C,MIN(F0,H0))
              D = MAX(D,MAX(F1,H1))

*     Update [mu1,mun] via bisection
              MU1 = BISECTION(MACHEPS,WRK(IDT),WRK(IET),ITNO+1,C,MU1,
     +              IRSLT)
              IF (IRSLT.EQ.-1) THEN
                  STATUS = -8
                  STEPERR = 18
                  GO TO 9999
              END IF
              IF (IRSLT.EQ.-2) THEN
                  STATUS = -9
                  STEPERR = 18
                  GO TO 9999
              END IF

              MUN = BISECTION(MACHEPS,WRK(IDT),WRK(IET),ITNO+1,MUN,D,
     +              IRSLT)
              IF (IRSLT.EQ.-1) THEN
                  STATUS = -10
                  STEPERR = 18
                  GO TO 9999
              END IF
              IF (IRSLT.EQ.-2) THEN
                  STATUS = -11
                  STEPERR = 18
                  GO TO 9999
              END IF


          END IF

   20 CONTINUE

      IF (ITNO.GT.MAXIT) THEN
          STATUS = -1
          ITNO = MAXIT
      END IF

 9999 CONTINUE

      IF ((PRECONTYPE.EQ.2) .OR. (PRECONTYPE.EQ.3)) THEN
          CALL DCOPY(LOCLEN,X,1,WRK(IZ),1)
          CALL PRECONR(WRK(IZ),X,IPAR)
      END IF

*  Set output parameters
      IPAR(11) = ITNO
      IPAR(12) = STATUS
      IPAR(13) = STEPERR
      DPAR(2) = EXITNORM
      DPAR(3) = MU1
      DPAR(4) = MUN
      DPAR(5) = 0.0D0
      DPAR(6) = 0.0D0

      RETURN

      END

      DOUBLE PRECISION FUNCTION BISECTION(DMACHEPS,A,B,K,Y,Z,IRSLT)
      IMPLICIT NONE

*  Computes the root of a function (the determinantal function
*  associated with the Lanczos's tridiagonal matrix) using the
*  bisection method.
*  Possible values of IRSLT: 0 - no floating-point errors
*                           -1 - underflow
*                           -2 - overflow
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
      DOUBLE PRECISION ONE
      PARAMETER (ONE=1.0D0)
      DOUBLE PRECISION TWO
      PARAMETER (TWO=2.0D0)
      INTEGER MAXIT
      PARAMETER (MAXIT=50)
*     ..
*     .. Scalar Arguments ..
      DOUBLE PRECISION DMACHEPS,Y,Z
      INTEGER IRSLT,K
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION A(*),B(*)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION P2X,P2Y,X
      INTEGER IT
*     ..
*     .. External Functions ..
      DOUBLE PRECISION DETFUN
      EXTERNAL DETFUN
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION OVERFLOW,UNDERFLOW
*     ..
*     .. Common blocks ..
      COMMON /MACHCONS1/UNDERFLOW,OVERFLOW
*     ..
      IRSLT = 0
      X = (Y+Z)/TWO
      IF (ABS(Y-Z).LE. (DMACHEPS* (ABS(Y)+ABS(Z)))) THEN
          BISECTION = X
          RETURN
      END IF

* Compute p2(y) once only
      P2Y = DETFUN(A,B,K,Y)
      IF (P2Y.EQ.UNDERFLOW) THEN
          BISECTION = X
          IRSLT = -1
          RETURN
      END IF
      IF (P2Y.EQ.OVERFLOW) THEN
          BISECTION = X
          IRSLT = -2
          RETURN
      END IF

      DO 10 IT = 1,MAXIT
          P2X = DETFUN(A,B,K,X)
          IF (P2X.EQ.UNDERFLOW) THEN
              BISECTION = X
              IRSLT = -1
              RETURN
          END IF
          IF (P2X.EQ.OVERFLOW) THEN
              BISECTION = X
              IRSLT = -2
              RETURN
          END IF

          IF (SIGN(ONE,P2X).NE.SIGN(ONE,P2Y)) THEN
* There is a root between x and y
* p2(y) remains the same for the next iteration
              Z = X

          ELSE IF (ABS(P2X).LT.DMACHEPS) THEN
* Close enought to zero, accept x as root
              BISECTION = X
              RETURN
          ELSE
* There is a root between x and z
* p2(y) becomes p2(x) for next iteration
              Y = X
              P2Y = P2X
          END IF

          IF (ABS(Y-Z).LE. (DMACHEPS* (ABS(Y)+ABS(Z)))) THEN
              BISECTION = X
              RETURN
          END IF
          X = (Y+Z)/TWO
   10 CONTINUE
      BISECTION = X
      RETURN

      END

      DOUBLE PRECISION FUNCTION DETFUN(A,B,K,X)
      IMPLICIT NONE


*  Compute determinant function at x; the squares of
*  the subdiagonal elements of T_{k} are stored in b
*     .. Scalar Arguments ..
      DOUBLE PRECISION X
      INTEGER K
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION A(*),B(*)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION P0,P1,P2
      INTEGER I
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION OLIMIT,OVERFLOW,ULIMIT,UNDERFLOW
*     ..
*     .. Common blocks ..
      COMMON /MACHCONS1/UNDERFLOW,OVERFLOW
      COMMON /MACHCONS2/ULIMIT,OLIMIT
*     ..
*     .. Parameters ..
      DOUBLE PRECISION ZERO
      PARAMETER (ZERO=0.0D0)
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS
*     ..
      P1 = A(1) - X
      P2 = (A(2)-X)*P1 - B(1)
      DO 10 I = 3,K
          P0 = P1
          P1 = P2
          P2 = (A(I)-X)*P1 - B(I-1)*P0
          IF ((ABS(P2).GT.ZERO) .AND. (ABS(P2).LE.ULIMIT)) THEN
              DETFUN = UNDERFLOW
              RETURN
          END IF
          IF (ABS(P2).GE.OLIMIT) THEN
              DETFUN = OVERFLOW
              RETURN
          END IF

   10 CONTINUE

      DETFUN = P2
      RETURN

      END
