      SUBROUTINE PROGRESS(LOCLEN,ITNO,NORMRES,X,RES,TRUERES)
      IMPLICIT NONE

*     .. Scalar Arguments ..
      DOUBLE PRECISION NORMRES
      INTEGER ITNO,LOCLEN
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX RES(*),TRUERES(*),X(*)
*     ..
*     .. External Subroutines ..
      EXTERNAL PRINTV
*     ..
      WRITE (6,FMT=9000) ITNO,NORMRES
*     WRITE (6,FMT=9010) 'X:'
*     CALL PRINTV(LOCLEN,X)
*     WRITE (6,FMT=9010) 'RES:'
*     CALL PRINTV(LOCLEN,RES)
*     WRITE (6,FMT=9010) 'TRUE RES:'
*     CALL PRINTV(LOCLEN,TRUERES)
      RETURN
 9000 FORMAT (/,I5,1X,D16.10)
 9010 FORMAT (/,A)
      END
