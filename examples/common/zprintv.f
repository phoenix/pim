      SUBROUTINE PRINTV(N,U)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      INTEGER N
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*)
*     ..
*     .. Local Scalars ..
      INTEGER I
*     ..
      WRITE (6,FMT=9000) (U(I),I=1,N)
      RETURN

 9000 FORMAT (2('(', D14.8, ',', D14.8, ')'),1x)
      END
