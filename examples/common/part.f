      SUBROUTINE PART(N,P,RANGES)
      IMPLICIT NONE

*     .. Scalar Arguments ..
      INTEGER N,P
*     ..
*     .. Array Arguments ..
      INTEGER RANGES(3,*)
*     ..
*     .. Local Scalars ..
      INTEGER D,DELTA,I,R,REM,S
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC MOD
*     ..
      DELTA = N/P
      REM = MOD(N,P)

      IF (REM.EQ.0) THEN
          S = 0

      ELSE
          S = 1
      END IF

      R = 1
      DO 10 I = 1,P
          RANGES(1,I) = R
          D = DELTA + S
          RANGES(2,I) = D
          R = R + D
          RANGES(3,I) = R - 1
          IF (REM.EQ.1) THEN
              S = 0

          ELSE
              REM = REM - 1
          END IF

   10 CONTINUE

      RETURN

      END
