# Builds the sources and executables in the "common" directory

# List the sources to build into the library
set(PIM_EXAMPLES_COMMON_SOURCES
    timer.f
    part.f
)

if (PIM_DATATYPE STREQUAL "single")
    set(PIM_EXAMPLES_SOURCES sgcpde.f sprintv.f sprogress.f sreport.f sulscoef.f svprod.f swlscoef.f)
elseif (PIM_DATATYPE STREQUAL "double")
    set(PIM_EXAMPLES_SOURCES dgcpde.f dprintv.f dprogress.f dreport.f dulscoef.f dvprod.f dwlscoef.f)
elseif (PIM_DATATYPE STREQUAL "complex")
    set(PIM_EXAMPLES_SOURCES cprintv.f cprogress.f creport.f cvprod.f sulscoef.f swlscoef.f dulscoef.f dwlscoef.f)
elseif (PIM_DATATYPE STREQUAL "dcomplex")
    set(PIM_EXAMPLES_SOURCES zprintv.f zprogress.f zreport.f zvprod.f dulscoef.f dwlscoef.f)
else()
    message(FATAL_ERROR "PIM_DATATYPE not recognised. Was" ${PIM_DATATYPE})
endif()

# Add the source files to a "PIM-LibraryExamples" library
# Set the library type based on the ENABLE_SHARED_PIM_LIB flag
if(${ENABLE_SHARED_PIM_LIB})
    message(STATUS "We will build SHARED PIM-LibraryExamples library")
    add_library(PIM-LibraryExamples SHARED ${PIM_EXAMPLES_COMMON_SOURCES} ${PIM_EXAMPLES_SOURCES})
    set_target_properties(PIM-LibraryExamples
        PROPERTIES
        POSITION_INDEPENDENT_CODE ON
        OUTPUT_NAME "PIM-LibraryExamples"
        DEBUG_POSTFIX "_d"
        MACOSX_RPATH ON
        WINDOWS_EXPORT_ALL_SYMBOLS ON
        )
else()
    message(STATUS "We will build STATIC PIM-LibraryExamples library")
    add_library(PIM-LibraryExamples STATIC ${PIM_EXAMPLES_COMMON_SOURCES} ${PIM_EXAMPLES_SOURCES})
    set_target_properties(PIM-LibraryExamples
        PROPERTIES
        POSITION_INDEPENDENT_CODE OFF
        ARCHIVE_OUTPUT_NAME "PIM-LibraryExamples"
        DEBUG_POSTFIX "_d"
        )

endif()
