      SUBROUTINE PROGRESS(LOCLEN,ITNO,NORMRES,X,RES,TRUERES)
      IMPLICIT NONE

*     .. Scalar Arguments ..
      REAL NORMRES
      INTEGER ITNO,LOCLEN
*     ..
*     .. Array Arguments ..
      REAL RES(*),TRUERES(*),X(*)
*     ..
*     .. External Subroutines ..
      EXTERNAL PRINTV
*     ..
*     WRITE (6,FMT=9000) ITNO,NORMRES
*     WRITE (6,FMT=9010) 'X:'
*     CALL PRINTV(LOCLEN,X)
*     WRITE (6,FMT=9010) 'RES:'
*     CALL PRINTV(LOCLEN,RES)
*     WRITE (6,FMT=9010) 'TRUE RES:'
*     CALL PRINTV(LOCLEN,TRUERES)
      RETURN
 9000 FORMAT (/,I5,1X,E14.8)
 9010 FORMAT (/,A)
      END
