      SUBROUTINE REPORT(NAME,IPAR,SPAR,ET,X)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      REAL ET
      CHARACTER*(*) NAME
*     ..
*     .. Array Arguments ..
      REAL SPAR(*),X(*)
      INTEGER IPAR(*)
*     ..
*     .. External Subroutines ..
      EXTERNAL PRINTV
*     ..
      WRITE (6,FMT=9000) NAME,IPAR(2),IPAR(11),IPAR(12),IPAR(13),
     +  SPAR(2),ET
      CALL PRINTV(IPAR(4),X)


      RETURN

 9000 FORMAT (/,A,': N=',I6,/,2X,'k=',I6,' status=',I6,' steperr=',I6,
     +       ' norm=',E14.8,/,2X,'Execution time(s)= ',F15.8)
      END
