* Generates the coefficients of the five-point fd for
* the convection-diffusion equation
*
*          {  2     2  }
*          { d u   d u }      du      du
* -epsilon { --- + --- } + vx -- + vy -- = 0  on [0,1]x[0,1]
*          {   2     2 }      dx      dy
*          { dx    dy  }
*
*    vx=cos(alpha), vy=sin(alpha)
*       2  2
*    u=x +y on boundary
      SUBROUTINE GENCOEFS(X0,X1,Y0,Y1,EPSILON,ALPHA,LDC,COEFS,RHS)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON
      INTEGER LDC,X0,X1,Y0,Y1
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION COEFS(LDC,*),RHS(*)
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K
*     ..
*     .. External Subroutines ..
      EXTERNAL PREC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC DBLE
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      NX1 = NX - 1
      NY1 = NY - 1
      DHX = 1.0D0/DBLE(NX1)
      DHY = 1.0D0/DBLE(NY1)
      DHX2 = DHX*DHX
      DHY2 = DHY*DHY
      DHXY2 = DHX2*DHY2
      K = 1
      DO 20 J = X0,X1
          DO 10 I = Y0,Y1
              CALL PREC(I,J,EPSILON,ALPHA,COEFS(K,1),COEFS(K,2),
     +                  COEFS(K,3),COEFS(K,4),COEFS(K,5),RHS(K))
              K = K + 1
   10     CONTINUE
   20 CONTINUE
      RETURN

      END
      SUBROUTINE PREC(I,J,EPSILON,ALPHA,C,N,S,E,W,RHS)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,C,E,EPSILON,N,RHS,S,W
      INTEGER I,J
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION X,XB,Y,YB
*     ..
*     .. External Functions ..
      DOUBLE PRECISION CCOEF,ECOEF,F,NCOEF,SCOEF,U,WCOEF
      EXTERNAL CCOEF,ECOEF,F,NCOEF,SCOEF,U,WCOEF
*     ..
*     .. External Subroutines ..
      EXTERNAL XYVAL
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      CALL XYVAL(I,J,X,Y)
* Set rhs value to yero
      RHS = 0.0D0
* Centre
      C = CCOEF(X,Y,EPSILON,ALPHA)
* North & South
      IF (I.EQ.1) THEN
          N = NCOEF(X,Y,EPSILON,ALPHA)
          S = 0.0D0
          CALL XYVAL(I-1,J,XB,YB)
          RHS = RHS - SCOEF(X,Y,EPSILON,ALPHA)*U(XB,YB)

      ELSE IF (I.EQ.NY) THEN
          N = 0.0D0
          S = SCOEF(X,Y,EPSILON,ALPHA)
          CALL XYVAL(I+1,J,XB,YB)
          RHS = RHS - NCOEF(X,Y,EPSILON,ALPHA)*U(XB,YB)

      ELSE
          N = NCOEF(X,Y,EPSILON,ALPHA)
          S = SCOEF(X,Y,EPSILON,ALPHA)
      END IF
* East & West
      IF (J.EQ.1) THEN
          E = ECOEF(X,Y,EPSILON,ALPHA)
          W = 0.0D0
          CALL XYVAL(I,J-1,XB,YB)
          RHS = RHS - WCOEF(X,Y,EPSILON,ALPHA)*U(XB,YB)

      ELSE IF (J.EQ.NX) THEN
          E = 0.0D0
          W = WCOEF(X,Y,EPSILON,ALPHA)
          CALL XYVAL(I,J+1,XB,YB)
          RHS = RHS - ECOEF(X,Y,EPSILON,ALPHA)*U(XB,YB)

      ELSE
          E = ECOEF(X,Y,EPSILON,ALPHA)
          W = WCOEF(X,Y,EPSILON,ALPHA)
      END IF
* RHS
      RHS = RHS + (DHXY2*F(X,Y))
      RETURN

      END
      DOUBLE PRECISION FUNCTION CCOEF(X,Y,EPSILON,ALPHA)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON,X,Y
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC COS,SIN
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      CCOEF = 2.0D0*EPSILON* (DHY2+DHX2) - DHX*DHY2*COS(ALPHA) -
     +        DHX2*DHY*SIN(ALPHA)
      RETURN

      END
      DOUBLE PRECISION FUNCTION NCOEF(X,Y,EPSILON,ALPHA)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON,X,Y
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC SIN
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      NCOEF = DHX*DHY2*SIN(ALPHA) - EPSILON*DHX2
      RETURN

      END
      DOUBLE PRECISION FUNCTION SCOEF(X,Y,EPSILON,ALPHA)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON,X,Y
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      SCOEF = -EPSILON*DHX2
      RETURN

      END
      DOUBLE PRECISION FUNCTION ECOEF(X,Y,EPSILON,ALPHA)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON,X,Y
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC COS
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      ECOEF = DHX2*DHY*COS(ALPHA) - EPSILON*DHY2
      RETURN

      END
      DOUBLE PRECISION FUNCTION WCOEF(X,Y,EPSILON,ALPHA)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION ALPHA,EPSILON,X,Y
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      WCOEF = -EPSILON*DHY2
      RETURN

      END
      DOUBLE PRECISION FUNCTION U(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      U = X*X + Y*Y
      RETURN

      END
      DOUBLE PRECISION FUNCTION DUX(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      DUX = 2.0D0*X
      RETURN

      END
      DOUBLE PRECISION FUNCTION DUY(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      DUY = 2.0D0*Y
      RETURN

      END
      DOUBLE PRECISION FUNCTION DUXX(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      DUXX = 2.0D0
      RETURN

      END
      DOUBLE PRECISION FUNCTION DUYY(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      DUYY = 2.0D0
      RETURN

      END
      DOUBLE PRECISION FUNCTION F(X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
*     ..
      F = 0.0D0
      RETURN

      END
      SUBROUTINE XYVAL(I,J,X,Y)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      DOUBLE PRECISION X,Y
      INTEGER I,J
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER NX,NX1,NY,NY1
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC DBLE
*     ..
*     .. Common blocks ..
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
*     ..
      X = DBLE(J)/DBLE(NX1)
      Y = DBLE(I)/DBLE(NY1)
      RETURN

      END
