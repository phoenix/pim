      REAL FUNCTION TIMER()
      IMPLICIT NONE

* This function returns the execution time in seconds.
*
* It is NOT portable and MUST be adjusted for different
* systems. Some examples are:
*
*   Cray Y-MP:
*     timer=second()
*
*   IBM RS/6000:
*     timer=float(mclock())/100.0
*
*   UNIX etime function:
*     dimension tarray(2)
*     external etime
*     real etime
*     timer=etime(tarray)
*
*     .. Local Arrays ..
      REAL TARRAY(2)
*     ..
*     .. External Functions ..
      REAL ETIME
*     ..
      TIMER = ETIME(TARRAY)

      RETURN

      END
