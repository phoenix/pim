      PROGRAM ZHYBRID
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER BASIS
      PARAMETER (BASIS=5)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LOCLEN)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION MU1,MUN,TOL
      REAL ET,ET0,ET1
      INTEGER C,I,IERR,J,J0,J1,JJ,MAXIT,MYID,MYN,N,NPROCS,PRET,STOPT
*     ..
*     .. Local Arrays ..
      DOUBLE COMPLEX B(LOCLEN),WRK(LWRK),X(LOCLEN)
      DOUBLE PRECISION DPAR(DPARSIZ)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDZNRM2
      REAL TIMER
      EXTERNAL PDZNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,MATVEC,MPI_COMM_RANK,MPI_COMM_SIZE,
     +         MPI_FINALIZE,MPI_INIT,PART,PIMDSETPAR,PIMZCHEBYSHEV,
     +         PIMZRGMRESEV,PROGRESS,PZSUM,REPORT,ZINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC DCMPLX,INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      N = LDA
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0D-10
      MAXIT = INT(N/4)

      CALL PART(N,NPROCS,RANGES)
      MYN = RANGES(2,MYID+1)

* Create A
      J0 = RANGES(1,MYID+1)
      J1 = RANGES(3,MYID+1)
      DO 30 I = 1,N
          JJ = 1
          DO 20 J = J0,J1
              IF (I.EQ.J) THEN
                  A(I,JJ) = DCMPLX(2.0D0,0.0D0)

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,JJ) = DCMPLX(2.0D0,1.0D0)

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,JJ) = DCMPLX(-1.0D0,1.0D0)

              ELSE
                  A(I,JJ) = DCMPLX(0.0D0,0.0D0)
              END IF

              JJ = JJ + 1
   20     CONTINUE
   30 CONTINUE
      CALL ZINIT(MYN,DCMPLX(1.0D0,0.0D0),X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      IPAR(4) = MYN
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          JJ = 1
          DO 40 I = J0,J1
              Q1(JJ) = 1.0D0/A(I,JJ)
              JJ = JJ + 1
   40     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          JJ = 1
          DO 50 I = J0,J1
              Q2(JJ) = 1.0D0/A(I,JJ)
              JJ = JJ + 1
   50     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          JJ = 1
          DO 60 I = J0,J1
              Q1(JJ) = 1.0D0/SQRT(A(I,JJ))
              Q2(JJ) = Q1(JJ)
              JJ = JJ + 1
   60     CONTINUE
      END IF

      WRITE (6,FMT=9010) 'dense','sequential',' RGMRESEV and CHEBYSHEV'

* HYBRID
      CALL ZINIT(MYN,DCMPLX(0.0D0,0.0D0),X,1)
      ET0 = TIMER()
      DO 160 I = 1,MAXIT
* RGMRESEV
          CALL PIMDSETPAR(IPAR,DPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,
     +                    STOPT,3,TOL)
          CALL PIMZRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PZSUM,
     +                      PDZNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 170
          END IF

* CHEBYSHEV
          CALL PIMDSETPAR(IPAR,DPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,
     +                    STOPT,5,TOL)
          MU1 = DPAR(3)
          MUN = DPAR(4)
          DPAR(3) = 1.0D0 - MUN
          DPAR(4) = 1.0D0 - MU1
          CALL PIMZCHEBYSHEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PZSUM,
     +                       PDZNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 170
          END IF
  160 CONTINUE
  170 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,DPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,/)
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX A(LDA,LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Arrays ..
      DOUBLE COMPLEX R(LOCLEN),S(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL PZMV
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
*     ..
      CALL PZMV(IPAR(6),IPAR(7),RANGES,IPAR(1),IPAR(2),IPAR(4),A,U,V,R,
     +          S)
      RETURN
      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX Q1(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL ZCOPY,ZVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL ZCOPY(IPAR(4),U,1,V,1)
      CALL ZVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX Q2(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL ZCOPY,ZVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL ZCOPY(IPAR(4),U,1,V,1)
      CALL ZVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
