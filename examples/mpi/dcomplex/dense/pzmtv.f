      SUBROUTINE PZMTV(NPROCS,MYID,RANGES,LDA,N,MYN,A,U,V,W)
      IMPLICIT NONE

      INCLUDE 'mpif.h'

*     .. Scalar Arguments ..
      INTEGER LDA,MYID,MYN,N,NPROCS
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX A(LDA,*),U(*),V(*),W(*)
      INTEGER RANGES(3,*)
*     ..
*     .. Local Scalars ..
      INTEGER I,IERR,J,K,MSGLEN,MYID1,T,TF,TI,TN
*     ..
*     .. Local Arrays ..
      INTEGER TR(2)
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC CONJG,DCMPLX
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_BCAST,ZINIT
*     ..
      MYID1 = MYID + 1
* Compute v with local u vector
      TI = RANGES(1,MYID1)
      TF = RANGES(3,MYID1)
      CALL ZINIT(MYN,DCMPLX(0.0D0,0.0D0),V,1)
      DO 20 J = 1,MYN
          K = 1
          DO 10 I = TI,TF
              V(J) = V(J) + CONJG(A(I,J))*U(K)
              K = K + 1
   10     CONTINUE
   20 CONTINUE

* Update v with remote vectors
      DO 50 T = 0,NPROCS - 1
          IF (T.EQ.MYID) THEN

* Broadcast local u vector to other processors
              MYID1 = MYID + 1
              TI = RANGES(1,MYID1)
              TF = RANGES(3,MYID1)
              TR(1) = TI
              TR(2) = TF
              MSGLEN = 2
              CALL MPI_BCAST(TR,MSGLEN,MPI_INTEGER,T,MPI_COMM_WORLD,
     +                       IERR)

              MSGLEN = MYN
              CALL MPI_BCAST(U,MSGLEN,MPI_DOUBLE_COMPLEX,T,
     +                       MPI_COMM_WORLD,IERR)

          ELSE

* Receive u from other processors
              MSGLEN = 2
              CALL MPI_BCAST(TR,MSGLEN,MPI_INTEGER,T,MPI_COMM_WORLD,
     +                       IERR)

              TI = TR(1)
              TF = TR(2)
              TN = TF - TI + 1
              MSGLEN = TN
              CALL MPI_BCAST(W,MSGLEN,MPI_DOUBLE_COMPLEX,T,
     +                       MPI_COMM_WORLD,IERR)

              DO 40 J = 1,MYN
                  K = 1
                  DO 30 I = TI,TF
                      V(J) = V(J) + CONJG(A(I,J))*W(K)
                      K = K + 1
   30             CONTINUE
   40         CONTINUE
          END IF

   50 CONTINUE

      RETURN

      END
