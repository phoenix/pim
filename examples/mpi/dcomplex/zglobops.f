      SUBROUTINE PZSUM(ISIZE,X,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Arrays ..
      DOUBLE COMPLEX WRK(10)
*     ..
*     .. Local Scalars ..
      INTEGER IERR
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE,ZCOPY
*     ..
      CALL MPI_ALLREDUCE(X,WRK,ISIZE,MPI_DOUBLE_COMPLEX,MPI_SUM,
     +                   MPI_COMM_WORLD,IERR)
      CALL ZCOPY(ISIZE,WRK,1,X,1)

      RETURN

      END

      DOUBLE PRECISION FUNCTION PDZNRM2(LOCLEN,U,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION PSUM
      INTEGER IERR
*     ..
*     .. External Functions ..
      DOUBLE COMPLEX ZDOTC
      EXTERNAL ZDOTC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION WRK(1)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE
*     ..
      PSUM = ABS(ZDOTC(LOCLEN,U,1,U,1))
      CALL MPI_ALLREDUCE(PSUM,WRK,1,MPI_DOUBLE_PRECISION,MPI_SUM,
     +                   MPI_COMM_WORLD,IERR)
      PDZNRM2 = SQRT(WRK(1))
      RETURN

      END
