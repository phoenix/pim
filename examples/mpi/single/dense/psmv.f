      SUBROUTINE PSMV(NPROCS,MYID,RANGES,LDA,N,MYN,A,U,V,R,S)
      IMPLICIT NONE

      INCLUDE 'mpif.h'

*     .. Scalar Arguments ..
      INTEGER LDA,MYID,MYN,N,NPROCS
*     ..
*     .. Array Arguments ..
      REAL A(LDA,*),R(*),S(*),U(*),V(*)
      INTEGER RANGES(3,*)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K,T,TARGET,TF,TI,TN,IERR
*     ..
*     .. External Subroutines ..
      EXTERNAL SINIT,SCOPY,MPI_REDUCE
*     ..
      DO 40 T = 1,NPROCS
          TI = RANGES(1,T)
          TN = RANGES(2,T)
          TF = RANGES(3,T)

          CALL SINIT(TN,0.0D0,R,1)

          DO 30 J = 1,MYN
              K = 0
              DO 20 I = TI,TF
                  K = K + 1
                  R(K) = R(K) + A(I,J)*U(J)
   20         CONTINUE
   30     CONTINUE

* Make target=t-1 since processors are zero-relative
          TARGET = T - 1
          CALL MPI_REDUCE(R,S,TN,MPI_REAL,MPI_SUM,
     +                    TARGET,MPI_COMM_WORLD,IERR)
          IF (MYID.EQ.TARGET) CALL SCOPY(MYN,S,1,V,1)

   40 CONTINUE

      RETURN

      END
