      PROGRAM SDIAG
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL
      INTEGER C,I,IERR,J,J0,J1,JJ,MAXIT,MYID,MYN,N,NPROCS,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      REAL B(LOCLEN),SPAR(SPARSIZ),WRK(LWRK),X(LOCLEN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,MATVEC,MPI_COMM_RANK,MPI_COMM_SIZE,
     +         MPI_FINALIZE,MPI_INIT,PART,PIMSBICG,PIMSBICGSTAB,PIMSCG,
     +         PIMSCGEV,PIMSCGNE,PIMSCGNR,PIMSCGS,PIMSQMR,PIMSRBICGSTAB,
     +         PIMSRGCR,PIMSRGMRES,PIMSRGMRESEV,PIMSSETPAR,PIMSTFQMR,
     +         PROGRESS,PSSUM,REPORT,SINIT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      N = LDA
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = INT(N/2)

      CALL PART(N,NPROCS,RANGES)
      MYN = RANGES(2,MYID+1)

* Create A
      J0 = RANGES(1,MYID+1)
      J1 = RANGES(3,MYID+1)
      DO 30 I = 1,N
          JJ = 1
          DO 20 J = J0,J1
              IF (I.EQ.J) THEN
                  A(I,JJ) = 4.0

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,JJ) = 1.0

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,JJ) = 1.0

              ELSE
                  A(I,JJ) = 0.0
              END IF

              JJ = JJ + 1
   20     CONTINUE
   30 CONTINUE
      CALL SINIT(MYN,1.0,X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      IPAR(4) = MYN
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          JJ = 1
          DO 40 I = J0,J1
              Q1(JJ) = 1.0/A(I,JJ)
              JJ = JJ + 1
   40     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          JJ = 1
          DO 50 I = J0,J1
              Q2(JJ) = 1.0/A(I,JJ)
              JJ = JJ + 1
   50     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          JJ = 1
          DO 60 I = J0,J1
              Q1(JJ) = 1.0/SQRT(A(I,JJ))
              Q2(JJ) = Q1(JJ)
              JJ = JJ + 1
   60     CONTINUE
      END IF

      WRITE (6,FMT=9010) 'dense','parallel','diagonal'

* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCG(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* CGEV
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,4)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGS(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,V,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                   PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGCR(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)

* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PSSUM,
     +             PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,PSNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,' preconditioning.',/)
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Arrays ..
      REAL R(LOCLEN),S(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMV
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
*     ..
      CALL PSMV(IPAR(6),IPAR(7),RANGES,IPAR(1),IPAR(2),IPAR(4),A,U,V,R,
     +          S)
      RETURN
      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Arrays ..
      REAL R(LOCLEN),S(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMTV
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
*     ..
      CALL PSMTV(IPAR(6),IPAR(7),RANGES,IPAR(1),IPAR(2),IPAR(4),A,U,V,R,
     +           S)
      RETURN
      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q1(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q2(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
