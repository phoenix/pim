      PROGRAM SHYBRID
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LDC)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      REAL DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,M,MYLX,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),ECOEF(LLY),GAMMA(NGAMMA),Q1(LDC),Q2(LDC),
     +     WCOEF(LLY)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,MU1,MUN,TOL,VA,VB
      INTEGER C,I,IERR,IX0,IX1,MAXIT,MYID,MYN,N,NPROCS,PRET,STOPT
*     ..
*     .. Local Arrays ..
      REAL B(LDC),SPAR(SPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ),RANGES(3,MXPROCS)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,GENCOEFS,MATVEC,MPI_COMM_RANK,MPI_COMM_SIZE,
     +         MPI_FINALIZE,MPI_INIT,PART,PIMSCHEBYSHEV,PIMSRGMRESEV,
     +         PIMSSETPAR,PROGRESS,PSSUM,REPORT,SEXCHCOEF,SINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0004/ECOEF,WCOEF
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0E-5
      MAXIT = INT(N/2)
      VA = 1.0E-1
      VB = -ACOS(-1.0)/6.0

      CALL PART(LX,NPROCS,RANGES)
      MYLX = RANGES(2,MYID+1)
      MYN = LY*MYLX

      NX = LLX
      NY = LLY
      IX0 = RANGES(1,MYID+1)
      IX1 = RANGES(3,MYID+1)
      CALL GENCOEFS(IX0,IX1,1,LY,VA,VB,LDC,COEFS,B)
      CALL SINIT(MYN,1.0E0,X,1)
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Exchange East and West coefficients with neighbours
      CALL SEXCHCOEF(NPROCS,MYID,LDC,LY,COEFS,ECOEF,WCOEF)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 10 I = 1,MYN
              Q1(I) = 1.0/COEFS(I,1)
   10     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 20 I = 1,MYN
              Q2(I) = 1.0/COEFS(I,1)
   20     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 30 I = 1,MYN
              Q1(I) = 1.0/SQRT(COEFS(I,1))
              Q2(I) = Q1(I)
   30     CONTINUE
      END IF

      WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel',
     +  ' RGMRESEV and CHEBYSHEV'
* HYBRID
      CALL SINIT(MYN,0.0E0,X,1)
      ET0 = TIMER()
      DO 160 I = 1,MAXIT
* RGMRESEV
          CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,
     +                    STOPT,3,TOL)
          CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                      PSNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 170
          END IF

* CHEBYSHEV
          CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,
     +                    STOPT,5,TOL)
          MU1 = SPAR(3)
          MUN = SPAR(4)
          SPAR(3) = 1.0E0 - MUN
          SPAR(4) = 1.0E0 - MU1
          CALL PIMSCHEBYSHEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                       PSNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 170
          END IF
  160 CONTINUE
  170 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,SPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,/)
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. Local Arrays ..
      REAL UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMA/COEFS
*     ..
      CALL PSMVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),ECOEF(LLY),WCOEF(LLY)
*     ..
*     .. Local Arrays ..
      REAL UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0006/ECOEF,WCOEF
      COMMON /PIMA/COEFS
*     ..
      CALL PSMTVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,ECOEF,WCOEF,
     +              U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q1(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q2(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
