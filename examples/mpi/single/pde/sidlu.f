      PROGRAM SIDLU
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
*     ..
*     .. Scalars in Common ..
      REAL DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,MYLX,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),ECOEF(LLY),LU(LDC,2),WCOEF(LLY)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL,VA,VB
      INTEGER C,I,IERR,IX0,IX1,J,MAXIT,MYID,MYN,N,NPROCS,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      REAL B(LDC),SPAR(SPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ),RANGES(3,MXPROCS)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL GENCOEFS,IDLU,MATVEC,MPI_COMM_RANK,MPI_COMM_SIZE,
     +         MPI_FINALIZE,MPI_INIT,PART,PIMSBICG,PIMSBICGSTAB,PIMSCG,
     +         PIMSCGEV,PIMSCGNE,PIMSCGNR,PIMSCGS,PIMSQMR,PIMSRBICGSTAB,
     +         PIMSRGCR,PIMSRGMRES,PIMSRGMRESEV,PIMSSETPAR,PIMSTFQMR,
     +         PROGRESS,PSSUM,REPORT,SEXCHCOEF,SINIT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0004/ECOEF,WCOEF
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = INT(N/2)
      MAXIT = 200
      VA = 1.0E-1
      VB = -ACOS(-1.0)/6.0

      CALL PART(LX,NPROCS,RANGES)
      MYLX = RANGES(2,MYID+1)
      MYN = LY*MYLX

      NX = LLX
      NY = LLY
      IX0 = RANGES(1,MYID+1)
      IX1 = RANGES(3,MYID+1)
      CALL GENCOEFS(IX0,IX1,1,LY,VA,VB,LDC,COEFS,B)
      CALL SINIT(MYN,1.0E0,X,1)
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Exchange East and West coefficients with neighbours
      CALL SEXCHCOEF(NPROCS,MYID,LDC,LY,COEFS,ECOEF,WCOEF)

* Compute idlu
      DO 10 I = 1,MYN
          LU(I,1) = COEFS(I,1)
          LU(I,2) = COEFS(I,3)
   10 CONTINUE
      DO 30 I = 1,MYLX
          DO 20 J = (I-1)*LY + 2, (I-1)*LY + LY
              LU(J,2) = LU(J,2)/LU(J-1,1)
              LU(J,1) = LU(J,1) - LU(J,2)*COEFS(J-1,2)
   20     CONTINUE
   30 CONTINUE

      WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel','idlu(0)'

* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCG(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* CGEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGEV(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,4)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGS(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,V,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,
     +                   PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSRGCR(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)

* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +             PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL SINIT(IPAR(4),0.0,X,1)

      ET0 = TIMER()
      CALL PIMSTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,' preconditioning.')
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. Local Arrays ..
      REAL UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMA/COEFS
*     ..
      CALL PSMVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),ECOEF(LLY),WCOEF(LLY)
*     ..
*     .. Local Arrays ..
      REAL UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PSMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0006/ECOEF,WCOEF
      COMMON /PIMA/COEFS
*     ..
      CALL PSMTVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,ECOEF,WCOEF,
     +              U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE IDLU(U,V,IPAR)
      IMPLICIT NONE
*  Solve Ly=u
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      REAL LU(LDC,2)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K
*     ..
*     .. Local Arrays ..
      REAL COEFS(LDC,5),Y(LDC)
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      DO 20 I = 1,MYLX
          K = (I-1)*LY + 1
          Y(K) = U(K)
          DO 10 J = K + 1, (I-1)*LY + LY
              Y(J) = U(J) - LU(J,2)*Y(J-1)
   10     CONTINUE
   20 CONTINUE

*  Solve Uv=y
      DO 40 I = MYLX,1,-1
          K = (I-1)*LY + LY
          V(K) = Y(K)/LU(K,1)
          DO 30 J = K - 1, (I-1)*LY + 1,-1
              V(J) = (Y(J)-COEFS(J,2)*V(J+1))/LU(J,1)
   30     CONTINUE
   40 CONTINUE

      RETURN

      END
