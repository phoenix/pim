      SUBROUTINE SEXCHCOEF(NPROCS,MYID,LDC,L,COEFS,ECOEF,WCOEF)
      IMPLICIT NONE

      INCLUDE 'mpif.h'

* Exchange east and west coefficients with neighbour processes for
* matrix-transpose-vector product
* Coefficient indices: c=1,n=2,s=3,e=4,w=5
* Computation is done in j-direction (vertical) and then in i-direction
*     .. Scalar Arguments ..
      INTEGER L,LDC,MYID,NPROCS
*     ..
*     .. Array Arguments ..
      REAL COEFS(LDC,*),ECOEF(*),WCOEF(*)
*     ..
*     .. Local Scalars ..
      INTEGER FROM,IERR,LASTP,MSGTYPE,RID0,RID1,SID0,SID1,TO
*     ..
*     .. Local Arrays ..
      INTEGER ISTAT(MPI_STATUS_SIZE)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_IRECV,MPI_ISEND,MPI_WAIT
*     ..
      LASTP = NPROCS - 1

* 1st processor
      IF (MYID.EQ.0) THEN

* Send East coefficient to (myid+1)-th processor
          MSGTYPE = 1000
          TO = MYID + 1
          CALL MPI_ISEND(COEFS(1,4),L,MPI_REAL,TO,MSGTYPE,
     +                  MPI_COMM_WORLD,SID0,IERR)
* Receive West coefficient from (myid+1)-th processor
          MSGTYPE = 1001
          FROM = MYID + 1
          CALL MPI_IRECV(WCOEF,L,MPI_REAL,MPI_ANY_SOURCE,MSGTYPE,
     +                  MPI_COMM_WORLD,RID0,IERR)

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID0,ISTAT,IERR)

* Release message ID from isend
          CALL MPI_WAIT(SID0,ISTAT,IERR)

* Last processor
      ELSE IF (MYID.EQ.LASTP) THEN

* Send West coefficient to (myid-1)-th processor
          MSGTYPE = 1001
          TO = MYID - 1
          CALL MPI_ISEND(COEFS(1,5),L,MPI_REAL,TO,MSGTYPE,
     +                  MPI_COMM_WORLD,SID1,IERR)
* Receive East coefficient from (myid-1)-th processor
          MSGTYPE = 1000
          FROM = MYID - 1
          CALL MPI_IRECV(ECOEF,L,MPI_REAL,MPI_ANY_SOURCE,MSGTYPE,
     +                  MPI_COMM_WORLD,RID1,IERR)

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID1,ISTAT,IERR)

* Release message ID from isend
          CALL MPI_WAIT(SID1,ISTAT,IERR)

* Intermediate processors
      ELSE

* Send West coefficient to (myid-1)-th processor
          MSGTYPE = 1001
          TO = MYID - 1
          CALL MPI_ISEND(COEFS(1,5),L,MPI_REAL,TO,MSGTYPE,
     +                  MPI_COMM_WORLD,SID0,IERR)
* Receive East coefficient from (myid-1)-th processor
          MSGTYPE = 1000
          FROM = MYID - 1
          CALL MPI_IRECV(ECOEF,L,MPI_REAL,MPI_ANY_SOURCE,MSGTYPE,
     +                  MPI_COMM_WORLD,RID0,IERR)
* Send East coefficient to (myid+1)-th processor
          MSGTYPE = 1000
          TO = MYID + 1
          CALL MPI_ISEND(COEFS(1,4),L,MPI_REAL,TO,MSGTYPE,
     +                  MPI_COMM_WORLD,SID1,IERR)
* Receive West coefficient from (myid+1)-th processor
          MSGTYPE = 1001
          FROM = MYID + 1
          CALL MPI_IRECV(WCOEF,L,MPI_REAL,MPI_ANY_SOURCE,MSGTYPE,
     +                  MPI_COMM_WORLD,RID1,IERR)

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID0,ISTAT,IERR)
          CALL MPI_WAIT(RID1,ISTAT,IERR)

* Release message ID from isend
          CALL MPI_WAIT(SID0,ISTAT,IERR)
          CALL MPI_WAIT(SID1,ISTAT,IERR)

      END IF

      RETURN

      END
