      SUBROUTINE PSSUM(ISIZE,X,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      REAL X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Arrays ..
      REAL WRK(10)
*     ..
*     .. Local Scalars ..
      INTEGER IERR
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE,SCOPY
*     ..
      CALL MPI_ALLREDUCE(X,WRK,ISIZE,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,
     +                   IERR)
      CALL SCOPY(ISIZE,WRK,1,X,1)

      RETURN

      END

      REAL FUNCTION PSNRM2(LOCLEN,U,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      REAL U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Scalars ..
      REAL PSUM
      INTEGER IERR
*     ..
*     .. External Functions ..
      REAL SDOT
      EXTERNAL SDOT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC SQRT
*     ..
*     .. Local Arrays ..
      REAL WRK(1)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE
*     ..
      PSUM = SDOT(LOCLEN,U,1,U,1)
      CALL MPI_ALLREDUCE(PSUM,WRK,1,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,
     +                   IERR)
      PSNRM2 = SQRT(WRK(1))
      RETURN

      END
