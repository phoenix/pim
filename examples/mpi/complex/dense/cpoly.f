      PROGRAM CPOLY
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      COMPLEX A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
      REAL GAMMA(NGAMMA)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL
      INTEGER C,I,IERR,J,J0,J1,JJ,MAXIT,MYID,MYN,N,NPROCS,POLYT,PRET,
     +        STOPT,V
*     ..
*     .. Local Arrays ..
      COMPLEX B(LOCLEN),WRK(LWRK),X(LOCLEN)
      REAL SPAR(SPARSIZ)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSCNRM2,TIMER
      EXTERNAL PSCNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL CINIT,DULSCOEF,DWLSCOEF,MATVEC,MPI_COMM_RANK,
     +         MPI_COMM_SIZE,MPI_FINALIZE,MPI_INIT,PART,PCSUM,PIMCBICG,
     +         PIMCBICGSTAB,PIMCCG,PIMCCGNE,PIMCCGNR,PIMCCGS,PIMCQMR,
     +         PIMCRBICGSTAB,PIMCRGCR,PIMCRGMRES,PIMCRGMRESEV,PIMCTFQMR,
     +         PIMSSETPAR,POLYL,POLYR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC CMPLX,INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/RANGES
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      N = LDA
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = INT(N/2)
      POLYT = 1
      M = 1

      CALL PART(N,NPROCS,RANGES)
      MYN = RANGES(2,MYID+1)
      MAXIT = INT(N/2)

* Create A
      J0 = RANGES(1,MYID+1)
      J1 = RANGES(3,MYID+1)
      DO 30 I = 1,N
          JJ = 1
          DO 20 J = J0,J1
              IF (I.EQ.J) THEN
                  A(I,JJ) = CMPLX(4.0,-4.0)

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,JJ) = CMPLX(1.0,1.0)

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,JJ) = CMPLX(1.0,-1.0)

              ELSE
                  A(I,JJ) = CMPLX(0.0,0.0)
              END IF

              JJ = JJ + 1
   20     CONTINUE
   30 CONTINUE
      CALL CINIT(MYN,CMPLX(1.0,0.0),X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      IPAR(4) = MYN
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          JJ = 1
          DO 40 I = J0,J1
              Q1(JJ) = 1.0/A(I,JJ)
              JJ = JJ + 1
   40     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          JJ = 1
          DO 50 I = J0,J1
              Q2(JJ) = 1.0/A(I,JJ)
              JJ = JJ + 1
   50     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          JJ = 1
          DO 60 I = J0,J1
              Q1(JJ) = 1.0/SQRT(A(I,JJ))
              Q2(JJ) = Q1(JJ)
              JJ = JJ + 1
   60     CONTINUE
      END IF

* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 70 I = 1,M + 1
              GAMMA(I) = 1.0
   70     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL DWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL DULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9010) 'dense','parallel','Neumann polynomial',M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9010) 'dense','parallel',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9010) 'dense','parallel',
     +      'unweighted least-squares polynomial',M
      END IF

* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCCG(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,PSCNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PCSUM,
     +              PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCCGS(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,PSCNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,
     +                  PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,V,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,
     +                   PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,
     +                PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,
     +                  PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCRGCR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,PSCNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PCSUM,
     +              PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PCSUM,
     +              PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)

* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PCSUM,
     +             PSCNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL CINIT(IPAR(4),CMPLX(0.0,0.0),X,1)

      ET0 = TIMER()
      CALL PIMCTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PCSUM,PSCNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,' preconditioning (degree=',I2,')')
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
*     ..
*     .. Array Arguments ..
      COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      COMPLEX A(LDA,LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Arrays ..
      COMPLEX R(LOCLEN),S(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL PCMV
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
*     ..
      CALL PCMV(IPAR(6),IPAR(7),RANGES,IPAR(1),IPAR(2),IPAR(4),A,U,V,R,
     +          S)
      RETURN
      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
*     ..
*     .. Array Arguments ..
      COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      COMPLEX A(LDA,LOCLEN)
      INTEGER RANGES(3,MXPROCS)
*     ..
*     .. Local Arrays ..
      COMPLEX R(LOCLEN),S(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL PCMTV
*     ..
*     .. Common blocks ..
      COMMON /B0002/RANGES
      COMMON /PIMA/A
*     ..
      CALL PCMTV(IPAR(6),IPAR(7),RANGES,IPAR(1),IPAR(2),IPAR(4),A,U,V,R,
     +           S)
      RETURN
      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      COMPLEX Q1(LOCLEN)
      REAL GAMMA(NGAMMA)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      COMPLEX T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL CAXPY,CCOPY,CSCAL,CVPROD,MATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC CMPLX
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL CCOPY(LOCALN,U,1,T,1)
      CALL CVPROD(LOCALN,Q1,1,T,1)
      CALL CCOPY(LOCALN,T,1,V,1)
      CALL CSCAL(LOCALN,CMPLX(GAMMA(M+1)),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL CVPROD(LOCALN,Q1,1,W,1)
          CALL CAXPY(LOCALN,CMPLX(-1.0E0,0.0E0),W,1,V,1)
          CALL CAXPY(LOCALN,CMPLX(GAMMA(M-I+1)),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA/2)
*     ..
*     .. Array Arguments ..
      COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      COMPLEX Q2(LOCLEN)
      REAL GAMMA(NGAMMA)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      COMPLEX T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL CAXPY,CCOPY,CSCAL,CVPROD,MATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC CMPLX
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL CCOPY(LOCALN,U,1,T,1)
      CALL CVPROD(LOCALN,Q2,1,T,1)
      CALL CCOPY(LOCALN,T,1,V,1)
      CALL CSCAL(LOCALN,CMPLX(GAMMA(M+1)),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL CVPROD(LOCALN,Q2,1,W,1)
          CALL CAXPY(LOCALN,CMPLX(-1.0E0,0.0E0),W,1,V,1)
          CALL CAXPY(LOCALN,CMPLX(GAMMA(M-I+1)),T,1,V,1)
   10 CONTINUE
      RETURN

      END
