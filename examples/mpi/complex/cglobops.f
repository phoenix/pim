      SUBROUTINE PCSUM(ISIZE,X,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      COMPLEX X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Arrays ..
      COMPLEX WRK(10)
*     ..
*     .. Local Scalars ..
      INTEGER IERR
*     ..
*     .. External Subroutines ..
      EXTERNAL CCOPY,MPI_ALLREDUCE
*     ..
      CALL MPI_ALLREDUCE(X,WRK,ISIZE,MPI_COMPLEX,MPI_SUM,MPI_COMM_WORLD,
     +                   IERR)
      CALL CCOPY(ISIZE,WRK,1,X,1)

      RETURN

      END

      REAL FUNCTION PSCNRM2(LOCLEN,U,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      COMPLEX U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Scalars ..
      REAL PSUM
      INTEGER IERR
*     ..
*     .. External Functions ..
      COMPLEX CDOTC
      EXTERNAL CDOTC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ABS,SQRT
*     ..
*     .. Local Arrays ..
      REAL WRK(1)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE
*     ..
      PSUM = ABS(CDOTC(LOCLEN,U,1,U,1))
      CALL MPI_ALLREDUCE(PSUM,WRK,1,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,
     +                   IERR)
      PSCNRM2 = SQRT(WRK(1))
      RETURN

      END
