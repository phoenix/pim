      SUBROUTINE PDSUM(ISIZE,X,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION WRK(10)
*     ..
*     .. Local Scalars ..
      INTEGER IERR
*     ..
*     .. External Subroutines ..
      EXTERNAL DCOPY,MPI_ALLREDUCE
*     ..
      CALL MPI_ALLREDUCE(X,WRK,ISIZE,MPI_DOUBLE_PRECISION,MPI_SUM,
     +                   MPI_COMM_WORLD,IERR)
      CALL DCOPY(ISIZE,WRK,1,X,1)

      RETURN

      END

      DOUBLE PRECISION FUNCTION PDNRM2(LOCLEN,U,IPAR)

      INCLUDE 'mpif.h'
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION PSUM
      INTEGER IERR
*     ..
*     .. External Functions ..
      DOUBLE PRECISION DDOT
      EXTERNAL DDOT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC SQRT
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION WRK(1)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_ALLREDUCE
*     ..
      PSUM = DDOT(LOCLEN,U,1,U,1)
      CALL MPI_ALLREDUCE(PSUM,WRK,1,MPI_DOUBLE_PRECISION,MPI_SUM,
     +                   MPI_COMM_WORLD,IERR)
      PDNRM2 = SQRT(WRK(1))
      RETURN

      END
