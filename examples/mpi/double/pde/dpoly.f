      PROGRAM DPOLY
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,M,MYLX,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),ECOEF(LLY),GAMMA(NGAMMA),Q1(LDC),
     +                 Q2(LDC),WCOEF(LLY)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL,VA,VB
      REAL ET,ET0,ET1
      INTEGER C,I,IERR,IX0,IX1,MAXIT,MYID,MYN,N,NPROCS,POLYT,PRET,STOPT,
     +        V
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LDC),DPAR(DPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ),RANGES(3,MXPROCS)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DEXCHCOEF,DINIT,DULSCOEF,DWLSCOEF,GENCOEFS,MATVEC,
     +         MPI_COMM_RANK,MPI_COMM_SIZE,MPI_FINALIZE,MPI_INIT,PART,
     +         PDSUM,PIMDBICG,PIMDBICGSTAB,PIMDCG,PIMDCGEV,PIMDCGNE,
     +         PIMDCGNR,PIMDCGS,PIMDQMR,PIMDRBICGSTAB,PIMDRGCR,
     +         PIMDRGMRES,PIMDRGMRESEV,PIMDSETPAR,PIMDTFQMR,POLYL,POLYR,
     +         PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0004/ECOEF,WCOEF
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      POLYT = 1
      M = 1
      VA = 1.0D-1
      VB = -ACOS(-1.0D0)/6.0D0

      CALL PART(LX,NPROCS,RANGES)
      MYLX = RANGES(2,MYID+1)
      MYN = LY*MYLX

      NX = LLX
      NY = LLY
      IX0 = RANGES(1,MYID+1)
      IX1 = RANGES(3,MYID+1)
      CALL GENCOEFS(IX0,IX1,1,LY,VA,VB,LDC,COEFS,B)
      CALL DINIT(MYN,1.0D0,X,1)
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Exchange East and West coefficients with neighbours
      CALL DEXCHCOEF(NPROCS,MYID,LDC,LY,COEFS,ECOEF,WCOEF)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 10 I = 1,MYN
              Q1(I) = 1.0D0/COEFS(I,1)
   10     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 20 I = 1,MYN
              Q2(I) = 1.0D0/COEFS(I,1)
   20     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 30 I = 1,MYN
              Q1(I) = 1.0D0/SQRT(COEFS(I,1))
              Q2(I) = Q1(I)
   30     CONTINUE
      END IF

* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 40 I = 1,M + 1
              GAMMA(I) = 1.0D0
   40     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL DWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL DULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel',
     +      'Neumann polynomial',M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel',
     +      'unweighted least-squares polynomial',M
      END IF

* CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCG(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDBICG(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGS(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,V,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGCR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGNR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGNE(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)

* QMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDQMR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,' preconditioning (degree=',I2,')')
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5)
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PDMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMA/COEFS
*     ..
      CALL PDMVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),ECOEF(LLY),WCOEF(LLY)
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PDMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0006/ECOEF,WCOEF
      COMMON /PIMA/COEFS
*     ..
      CALL PDMTVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,ECOEF,WCOEF,
     +              U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE

*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q1(LDC)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LDC),W(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q1,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q1,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE

*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q2(LDC)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LDC),W(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q2,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q2,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN
      END
