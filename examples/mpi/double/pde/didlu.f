      PROGRAM DIDLU
      IMPLICIT NONE

*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

      INCLUDE 'mpif.h'

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER MXPROCS
      PARAMETER (MXPROCS=32)
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,MYLX,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),ECOEF(LLY),LU(LDC,2),WCOEF(LLY)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL,VA,VB
      REAL ET,ET0,ET1
      INTEGER C,I,IERR,IX0,IX1,J,MAXIT,MYID,MYN,N,NPROCS,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LDC),DPAR(DPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ),RANGES(3,MXPROCS)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DEXCHCOEF,DINIT,GENCOEFS,IDLU,MATVEC,MPI_COMM_RANK,
     +         MPI_COMM_SIZE,MPI_FINALIZE,MPI_INIT,PART,PDSUM,PIMDBICG,
     +         PIMDBICGSTAB,PIMDCG,PIMDCGEV,PIMDCGNE,PIMDCGNR,PIMDCGS,
     +         PIMDQMR,PIMDRBICGSTAB,PIMDRGCR,PIMDRGMRES,PIMDRGMRESEV,
     +         PIMDSETPAR,PIMDTFQMR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0004/ECOEF,WCOEF
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      CALL MPI_INIT(IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERR)

      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      VA = 1.0D-1
      VB = -ACOS(-1.0D0)/6.0D0

      CALL PART(LX,NPROCS,RANGES)
      MYLX = RANGES(2,MYID+1)
      MYN = LY*MYLX

      NX = LLX
      NY = LLY
      IX0 = RANGES(1,MYID+1)
      IX1 = RANGES(3,MYID+1)
      CALL GENCOEFS(IX0,IX1,1,LY,VA,VB,LDC,COEFS,B)
      CALL DINIT(MYN,1.0D0,X,1)
      IPAR(6) = NPROCS
      IPAR(7) = MYID
      CALL MATVEC(X,B,IPAR)

* Exchange East and West coefficients with neighbours
      CALL DEXCHCOEF(NPROCS,MYID,LDC,LY,COEFS,ECOEF,WCOEF)

* Compute idlu
      DO 10 I = 1,MYN
          LU(I,1) = COEFS(I,1)
          LU(I,2) = COEFS(I,3)
   10 CONTINUE
      DO 30 I = 1,MYLX
          DO 20 J = (I-1)*LY + 2, (I-1)*LY + LY
              LU(J,2) = LU(J,2)/LU(J-1,1)
              LU(J,1) = LU(J,1) - LU(J,2)*COEFS(J-1,2)
   20     CONTINUE
   30 CONTINUE

      WRITE (6,FMT=9010) '5-point f.-d. PDE','parallel','idlu(0)'

* CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCG(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDBICG(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGS(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,V,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDRGCR(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGNR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDCGNE(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)

* QMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDQMR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,MYN,MYN,C,NPROCS,MYID,PRET,STOPT,
     +                MAXIT,TOL)

      CALL DINIT(IPAR(4),0.0D0,X,1)

      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)

* Program finished, clean up MPI environment
      CALL MPI_FINALIZE(IERR)

      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data',' in ',A,
     +       ' mode.',/,'Using ',A,' preconditioning.')
      END

      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5)
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PDMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMA/COEFS
*     ..
      CALL PDMVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),ECOEF(LLY),WCOEF(LLY)
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION UEAST(LLY),UWEST(LLY)
*     ..
*     .. External Subroutines ..
      EXTERNAL PDMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /B0006/ECOEF,WCOEF
      COMMON /PIMA/COEFS
*     ..
      CALL PDMTVPDE(IPAR(6),IPAR(7),LDC,LX,LY,MYLX,COEFS,ECOEF,WCOEF,
     +              U,V,UEAST,UWEST)
      RETURN

      END
      SUBROUTINE IDLU(U,V,IPAR)
      IMPLICIT NONE
*  Solve Ly=u
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=8)
      INTEGER LLY
      PARAMETER (LLY=8)
      INTEGER LDC
      PARAMETER (LDC= (LLX*LLY)/2)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,MYLX
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION LU(LDC,2)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION COEFS(LDC,5),Y(LDC)
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY,MYLX
      COMMON /PIMQ1/LU
*     ..
      DO 20 I = 1,MYLX
          K = (I-1)*LY + 1
          Y(K) = U(K)
          DO 10 J = K + 1, (I-1)*LY + LY
              Y(J) = U(J) - LU(J,2)*Y(J-1)
   10     CONTINUE
   20 CONTINUE

*  Solve Uv=y
      DO 40 I = MYLX,1,-1
          K = (I-1)*LY + LY
          V(K) = Y(K)/LU(K,1)
          DO 30 J = K - 1, (I-1)*LY + 1,-1
              V(J) = (Y(J)-COEFS(J,2)*V(J+1))/LU(J,1)
   30     CONTINUE
   40 CONTINUE

      RETURN

      END
