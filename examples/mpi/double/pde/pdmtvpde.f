      SUBROUTINE PDMTVPDE(NPROCS,MYID,LDC,LX,LY,MYLX,COEFS,ECOEF,WCOEF,
     +                    U,V,UEAST,UWEST)
      IMPLICIT NONE

      INCLUDE 'mpif.h'

* Matrix-transpose-vector product
* Coefficient indices: c=1,n=2,s=3,e=4,w=5
* Computation is done in j-direction (vertical) and then in i-direction
*     .. Scalar Arguments ..
      INTEGER LX,LY,LDC,MYID,MYLX,NPROCS
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION COEFS(LDC,*),ECOEF(*),U(*),UEAST(*),UWEST(*),
     +                 V(*),WCOEF(*)
*     ..
*     .. Local Scalars ..
      INTEGER EI0,I,IERR,J,K,LX1,LY1,LASTP,MSGTYPE,RID0,RID1,SID0,SID1,
     +        TO,WI0
*     ..
*     .. Local Arrays ..
      INTEGER ISTAT(MPI_STATUS_SIZE)
*     ..
*     .. External Subroutines ..
      EXTERNAL MPI_IRECV,MPI_ISEND,MPI_WAIT
*     ..
      LX1 = LX - 1
      LY1 = LY - 1
      LASTP = NPROCS - 1
      WI0 = 1
      EI0 = (MYLX-1)*LY + 1

* 1st processor
      IF (MYID.EQ.0) THEN

* Send border U values to (myid+1)-th processor
          MSGTYPE = 2000
          TO = MYID + 1
          CALL MPI_ISEND(U(EI0),LY,MPI_DOUBLE_PRECISION,TO,MSGTYPE,
     +                   MPI_COMM_WORLD,SID0,IERR)
* Post to receive border U values from (myid+1)-th processor
          MSGTYPE = 2001
          CALL MPI_IRECV(UEAST,LY,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,
     +                   MSGTYPE,MPI_COMM_WORLD,RID0,IERR)

* 1st column
          V(1) = COEFS(1,1)*U(1) + COEFS(2,3)*U(2) + 
     +           COEFS(LY+1,5)*U(LY+1)
          DO 10 K = 2,LY1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY)
   10     CONTINUE
          V(LY) = COEFS(LY,1)*U(LY) + COEFS(LY1,2)*U(LY1) +
     +           COEFS(LY+LY,5)*U(LY+LY)

* Intermediate columns
          K = LY
          DO 30 J = 2,MYLX - 1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
              DO 20 I = 2,LY1
                  K = K + 1
                  V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +                   COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +                   COEFS(K-LY,4)*U(K-LY)
   20         CONTINUE
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
   30     CONTINUE

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID0,ISTAT,IERR)
* Compute with wcoef,ueast
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           WCOEF(1)*UEAST(1) + COEFS(K-LY,4)*U(K-LY)
          DO 40 I = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + WCOEF(I)*UEAST(I) +
     +               COEFS(K-LY,4)*U(K-LY)
   40     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           WCOEF(LY)*UEAST(LY) + COEFS(K-LY,4)*U(K-LY)

* Release message ID from isend
          CALL MPI_WAIT(SID0,ISTAT,IERR)
* Last processor
      ELSE IF (MYID.EQ.LASTP) THEN

* Send border U values to (myid-1)-th processor
          MSGTYPE = 2001
          TO = MYID - 1
          CALL MPI_ISEND(U(WI0),LY,MPI_DOUBLE_PRECISION,TO,MSGTYPE,
     +                   MPI_COMM_WORLD,SID1,IERR)

* Post to receive border U values from (myid-1)-th processor
          MSGTYPE = 2000
          CALL MPI_IRECV(UWEST,LY,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,
     +                   MSGTYPE,MPI_COMM_WORLD,RID1,IERR)

* Intermediate columns
          K = LY
          DO 70 J = 2,MYLX - 1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
              DO 60 I = 2,LY1
                  K = K + 1
                  V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +                   COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +                   COEFS(K-LY,4)*U(K-LY)
   60         CONTINUE
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
   70     CONTINUE

* Last column
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K-LY,4)*U(K-LY)
          DO 80 J = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + COEFS(K-LY,4)*U(K-LY)
   80     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           COEFS(K-LY,4)*U(K-LY)

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID1,ISTAT,IERR)
* Compute with ecoef,uwest
          K = 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K+LY,5)*U(K+LY) + ECOEF(1)*UWEST(1)
          DO 50 I = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +               ECOEF(I)*UWEST(I)
   50     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           COEFS(K+LY,5)*U(K+LY) + ECOEF(LY)*UWEST(LY)

* Release message ID from isend
          CALL MPI_WAIT(SID1,ISTAT,IERR)
* Intermediate processors
      ELSE

* Send border U values to (myid+1)-th processor
          MSGTYPE = 2000
          TO = MYID + 1
          CALL MPI_ISEND(U(EI0),LY,MPI_DOUBLE_PRECISION,TO,MSGTYPE,
     +                   MPI_COMM_WORLD,SID0,IERR)

* Post to receive border U values from (myid+1)-th processor
          MSGTYPE = 2001
          CALL MPI_IRECV(UEAST,LY,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,
     +                   MSGTYPE,MPI_COMM_WORLD,RID0,IERR)

* Send border U values to (myid-1)-th processor
          MSGTYPE = 2001
          TO = MYID - 1
          CALL MPI_ISEND(U(WI0),LY,MPI_DOUBLE_PRECISION,TO,MSGTYPE,
     +                   MPI_COMM_WORLD,SID1,IERR)

* Post to receive border U values from (myid-1)-th processor
          MSGTYPE = 2000
          CALL MPI_IRECV(UWEST,LY,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,
     +                   MSGTYPE,MPI_COMM_WORLD,RID1,IERR)

* Intermediate columns
          K = LY
          DO 110 J = 2,MYLX - 1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
              DO 100 I = 2,LY1
                  K = K + 1
                  V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +                   COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +                   COEFS(K-LY,4)*U(K-LY)
  100         CONTINUE
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +               COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
  110     CONTINUE

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID0,ISTAT,IERR)
* Compute with wcoef,ueast
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           WCOEF(1)*UEAST(1) + COEFS(K-LY,4)*U(K-LY)
          DO 120 I = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + WCOEF(I)*UEAST(I) +
     +               COEFS(K-LY,4)*U(K-LY)
  120     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           WCOEF(LY)*UEAST(LY) + COEFS(K-LY,4)*U(K-LY)

* Needs data,wait for completion of receive
          CALL MPI_WAIT(RID1,ISTAT,IERR)
* Compute with ecoef,uwest
          K = 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K+LY,5)*U(K+LY) + ECOEF(1)*UWEST(1)
          DO 90 I = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +               ECOEF(I)*UWEST(I)
   90     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           COEFS(K+LY,5)*U(K+LY) + ECOEF(LY)*UWEST(LY)

* Release message ID from isend
          CALL MPI_WAIT(SID0,ISTAT,IERR)
* Release message ID from isend
          CALL MPI_WAIT(SID1,ISTAT,IERR)
      END IF

      RETURN

      END
