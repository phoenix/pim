      PROGRAM ZHYBRID
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LOCLEN)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION MU1,MUN,TOL
      REAL ET,ET0,ET1
      INTEGER C,I,J,MAXIT,N,PRET,STOPT
*     ..
*     .. Local Arrays ..
      DOUBLE COMPLEX B(LOCLEN),WRK(LWRK),X(LOCLEN)
      DOUBLE PRECISION DPAR(DPARSIZ)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDZNRM2
      REAL TIMER
      EXTERNAL PDZNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,MATVEC,PIMDSETPAR,PIMZCHEBYSHEV,PIMZRGMRESEV,
     +         PROGRESS,PZSUM,REPORT,ZINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC DCMPLX,SQRT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      N = LDA
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0D-10
      MAXIT = 4*N
      DO 20 I = 1,N
          DO 10 J = 1,N
              IF (I.EQ.J) THEN
                  A(I,J) = DCMPLX(2.0D0,0.0D0)

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,J) = DCMPLX(2.0D0,1.0D0)

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,J) = DCMPLX(-1.0D0,1.0D0)

              ELSE
                  A(I,J) = DCMPLX(0.0D0,0.0D0)
              END IF

   10     CONTINUE
   20 CONTINUE
      CALL ZINIT(N,DCMPLX(1.0D0,0.0D0),X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0D0/A(I,I)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0D0/A(I,I)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0D0/SQRT(A(I,I))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF

      WRITE (6,FMT=9010) 'dense','sequential',' RGMRESEV and CHEBYSHEV'

* HYBRID
      CALL ZINIT(N,DCMPLX(0.0D0,0.0D0),X,1)
      ET0 = TIMER()
      DO 60 I = 1,MAXIT
* RGMRESEV
          CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,3,TOL)
          CALL PIMZRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PZSUM,
     +                      PDZNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 70
          END IF

* CHEBYSHEV
          CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,5,TOL)
          MU1 = DPAR(3)
          MUN = DPAR(4)
          DPAR(3) = 1.0D0 - MUN
          DPAR(4) = 1.0D0 - MU1
          CALL PIMZCHEBYSHEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PZSUM,
     +                       PDZNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 70
          END IF
   60 CONTINUE
   70 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,DPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL ZGEMV
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC DCMPLX
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL ZGEMV('N',IPAR(2),IPAR(2),DCMPLX(1.0),A,IPAR(1),U,1,
     +           DCMPLX(0.0),V,1)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX Q1(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL ZCOPY,ZVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL ZCOPY(IPAR(4),U,1,V,1)
      CALL ZVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE COMPLEX Q2(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL ZCOPY,ZVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL ZCOPY(IPAR(4),U,1,V,1)
      CALL ZVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
