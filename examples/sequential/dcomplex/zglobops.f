      SUBROUTINE PZSUM(ISIZE,X,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
      RETURN

      END

      DOUBLE PRECISION FUNCTION PDZNRM2(LOCLEN,U,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION DZNRM2
      EXTERNAL DZNRM2
*     ..
      PDZNRM2 = DZNRM2(LOCLEN,U,1)
      RETURN

      END
