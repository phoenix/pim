      PROGRAM SPOLY
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN),GAMMA(NGAMMA),Q1(LOCLEN),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL
      INTEGER C,I,J,MAXIT,N,POLYT,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      REAL B(LOCLEN),SPAR(SPARSIZ),WRK(LWRK),X(LOCLEN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL MATVEC,PIMSBICG,PIMSBICGSTAB,PIMSCG,PIMSCGEV,PIMSCGNE,
     +         PIMSCGNR,PIMSCGS,PIMSQMR,PIMSRBICGSTAB,PIMSRGCR,
     +         PIMSRGMRES,PIMSRGMRESEV,PIMSSETPAR,PIMSTFQMR,POLYL,POLYR,
     +         PROGRESS,PSSUM,REPORT,SINIT,SULSCOEF,SWLSCOEF,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      N = LDA
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = INT(N/2)
      POLYT = 1
      M = 1
      DO 20 I = 1,N
          DO 10 J = 1,N
              IF (I.EQ.J) THEN
                  A(I,J) = 4.0

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,J) = 1.0

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,J) = 1.0

              ELSE
                  A(I,J) = 0.0
              END IF

   10     CONTINUE
   20 CONTINUE
      CALL SINIT(N,1.0,X,1)
      IPAR(1) = LDA
      IPAR(2) = LOCLEN
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0/A(I,I)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0/A(I,I)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0/SQRT(A(I,I))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF
* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 60 I = 1,M + 1
              GAMMA(I) = 1.0
   60     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL SWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL SULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9010) 'dense','sequential','Neumann polynomial',
     +      M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9010) 'dense','sequential',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9010) 'dense','sequential',
     +      'unweighted least-squares polynomial',M
      END IF
* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCG(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* CGEV
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGEV(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,4)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGS(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                   PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGCR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)
* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +             PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning (degree=',I2,')')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL SGEMV('N',IPAR(2),IPAR(2),1.0,A,IPAR(1),U,1,0.0,V,1)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL SGEMV('T',IPAR(2),IPAR(2),1.0,A,IPAR(1),U,1,0.0,V,1)
      RETURN

      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      REAL GAMMA(NGAMMA),Q1(LOCLEN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      REAL T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL MATVEC,SAXPY,SCOPY,SSCAL,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL SCOPY(LOCALN,U,1,T,1)
      CALL SVPROD(LOCALN,Q1,1,T,1)
      CALL SCOPY(LOCALN,T,1,V,1)
      CALL SSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL SVPROD(LOCALN,Q1,1,W,1)
          CALL SAXPY(LOCALN,-1.0,W,1,V,1)
          CALL SAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      REAL GAMMA(NGAMMA),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      REAL T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL MATVEC,SAXPY,SCOPY,SSCAL,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL SCOPY(LOCALN,U,1,T,1)
      CALL SVPROD(LOCALN,Q2,1,T,1)
      CALL SCOPY(LOCALN,T,1,V,1)
      CALL SSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL SVPROD(LOCALN,Q2,1,W,1)
          CALL SAXPY(LOCALN,-1.0,W,1,V,1)
          CALL SAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
