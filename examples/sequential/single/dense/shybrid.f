      PROGRAM SHYBRID
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LOCLEN)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,MU1,MUN,TOL
      INTEGER C,I,J,MAXIT,N,PRET,STOPT
*     ..
*     .. Local Arrays ..
      REAL B(LOCLEN),SPAR(SPARSIZ),WRK(LWRK),X(LOCLEN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,MATVEC,PIMSCHEBYSHEV,PIMSRGMRESEV,PIMSSETPAR,
     +         PROGRESS,PSSUM,REPORT,SINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC SQRT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      N = LDA
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0E-5
      MAXIT = 4*N
      DO 20 I = 1,N
          DO 10 J = 1,N
              IF (I.EQ.J) THEN
                  A(I,J) = 2.0

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,J) = 2.0

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,J) = -1.0

              ELSE
                  A(I,J) = 0.0
              END IF

   10     CONTINUE
   20 CONTINUE
      CALL SINIT(N,1.0,X,1)
      IPAR(1) = LDA
      IPAR(2) = LOCLEN
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0/A(I,I)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0/A(I,I)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0/SQRT(A(I,I))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF

      WRITE (6,FMT=9010) 'dense','sequential',' RGMRESEV and CHEBYSHEV'

* HYBRID
      CALL SINIT(N,0.0E0,X,1)
      ET0 = TIMER()
      DO 60 I = 1,MAXIT
* RGMRESEV
          CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,3,TOL)
          CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                      PSNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 70
          END IF

* CHEBYSHEV
          CALL PIMSSETPAR(IPAR,SPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,5,TOL)
          MU1 = SPAR(3)
          MUN = SPAR(4)
          SPAR(3) = 1.0E0 - MUN
          SPAR(4) = 1.0E0 - MU1
          CALL PIMSCHEBYSHEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                       PSNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 70
          END IF
   60 CONTINUE
   70 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,SPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL SGEMV('N',IPAR(2),IPAR(2),1.0,A,IPAR(1),U,1,0.0,V,1)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q1(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q2(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
