      PROGRAM SHYBRID
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
* Reads a file in Harwell standard format for sparse matrices and
* solves the system with the PIM routines.
*
* The system solved has a rhs set such that the solution is the
*
* Code based on the sample given in pp. 14-16, Duff, I.S., Grimes,
* R.G. and Lewis, J.G., "Users' Guide for the Harwell-Boeing Sparse
* Matrix Collection (Release I)", Report TR/PA/92/86, CERFACS.
*  Set name of Harwell data file
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER INUNIT
      PARAMETER (INUNIT=1)
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER MAXN1
      PARAMETER (MAXN1=MAXN+1)
      INTEGER MAXNZ
      PARAMETER (MAXNZ=347)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=MAXN)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LOCLEN)
*     ..
*     .. Arrays in Common ..
      REAL Q1(MAXN),Q2(MAXN),VALUES(MAXNZ)
      INTEGER POINTR(MAXN1),ROWIND(MAXNZ)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,MU1,MUN,TOL
      INTEGER C,I,INDCRD,J,MAXIT,NCOL,NELTVL,NNZERO,NRHS,NROW,NRSHIX,
     +        PRET,PTRCRD,RHSCRD,STOPT,TOTCRD,VALCRD
      LOGICAL FEX
      CHARACTER*3 MXTYPE,RHSTYP
      CHARACTER*8 KEY
      CHARACTER*16 INDFMT,PTRFMT
      CHARACTER*20 RHSFMT,VALFMT
      CHARACTER*72 TITLE
      CHARACTER*80 INFIL
*     ..
*     .. Local Arrays ..
      REAL B(MAXN),SPAR(SPARSIZ),WRK(LWRK),X(MAXN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,MATVEC,PIMSCHEBYSHEV,PIMSRGMRESEV,PIMSSETPAR,
     +         PROGRESS,PSSUM,REPORT,SINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC SQRT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/VALUES
      COMMON /PIMAA/POINTR,ROWIND
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      INFIL = 'nos4'
*  Open the file infil
      INQUIRE (FILE=INFIL,EXIST=FEX)
      IF (.NOT.FEX) THEN
          PRINT *,'Input file not found. Exiting.'
          STOP

      END IF

      OPEN (INUNIT,FILE=INFIL,STATUS='old')
*  Read in header block
      READ (INUNIT,FMT=9010) TITLE,KEY,TOTCRD,PTRCRD,INDCRD,VALCRD,
     +  RHSCRD,MXTYPE,NROW,NCOL,NNZERO,NELTVL,PTRFMT,INDFMT,VALFMT,
     +  RHSFMT
      IF (RHSCRD.GT.0) THEN
          READ (INUNIT,FMT=9020) RHSTYP,NRHS,NRSHIX
      END IF
*  Read matrix structure
      READ (INUNIT,FMT=PTRFMT) (POINTR(I),I=1,NCOL+1)
      READ (INUNIT,FMT=INDFMT) (ROWIND(I),I=1,NNZERO)
      IF (VALCRD.GT.0) THEN
*  Read matrix values
          IF (MXTYPE(3:3).EQ.'A') THEN
              READ (INUNIT,FMT=VALFMT) (VALUES(I),I=1,NNZERO)

          ELSE
              PRINT *,'Matrix not in standard sparse format. Exiting.'
              CLOSE (INUNIT)
              STOP

          END IF

      ELSE
*  Just a pattern for the matrix, generate values
          DO 20 J = 1,NROW
              DO 10 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) THEN
                      VALUES(I) = 4.0

                  ELSE
                      VALUES(I) = -1.0
                  END IF

   10         CONTINUE
   20     CONTINUE
      END IF

      CLOSE (INUNIT)
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0E-5
      MAXIT = 4*NROW
      CALL SINIT(NROW,1.0,X,1)
      IPAR(1) = MAXN
      IPAR(2) = NROW
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 40 J = 1,NROW
              DO 30 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) Q1(J) = 1.0/VALUES(I)
   30         CONTINUE
   40     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 60 J = 1,NROW
              DO 50 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) Q2(J) = 1.0/VALUES(I)
   50         CONTINUE
   60     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 80 J = 1,NROW
              DO 70 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) THEN
                      Q1(J) = 1.0/SQRT(VALUES(I))
                      Q2(J) = Q1(J)
                  END IF

   70         CONTINUE
   80     CONTINUE
      END IF

      WRITE (6,FMT=9030) 'dense','sequential',' RGMRESEV and CHEBYSHEV'

* HYBRID
      CALL SINIT(NROW,0.0E0,X,1)
      ET0 = TIMER()
      DO 160 I = 1,MAXIT
* RGMRESEV
          CALL PIMSSETPAR(IPAR,SPAR,MAXNZ,NROW,NROW,NROW,C,-1,-1,PRET,
     +                    STOPT,3,TOL)

          CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                      PSNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 170
          END IF

* CHEBYSHEV
          CALL PIMSSETPAR(IPAR,SPAR,MAXNZ,NROW,NROW,NROW,C,-1,-1,PRET,
     +                    STOPT,5,TOL)
          MU1 = SPAR(3)
          MUN = SPAR(4)
          SPAR(3) = 1.0E0 - MUN
          SPAR(4) = 1.0E0 - MU1
          CALL PIMSCHEBYSHEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                       PSNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 170
          END IF
  160 CONTINUE
  170 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,SPAR,ET,X)

      STOP

 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT (A72,A8,/,5I14,/,A3,11X,4I14,/,2A16,2A20)
 9020 FORMAT (A3,11X,2I14)
 9030 FORMAT ('PIM 2.2',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER MAXN1
      PARAMETER (MAXN1=MAXN+1)
      INTEGER MAXNZ
      PARAMETER (MAXNZ=347)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL VALUES(MAXNZ)
      INTEGER POINTR(MAXN1),ROWIND(MAXNZ)
*     ..
*     .. Local Scalars ..
      INTEGER I,II,J,N
*     ..
*     .. External Subroutines ..
      EXTERNAL SINIT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/VALUES
      COMMON /PIMAA/POINTR,ROWIND
*     ..
      N = IPAR(2)
      CALL SINIT(N,0.0,V,1)
      DO 20 J = 1,N
          DO 10 I = POINTR(J),POINTR(J+1) - 1
              II = ROWIND(I)
              V(II) = V(II) + U(J)*VALUES(I)
   10     CONTINUE
   20 CONTINUE
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q1(MAXN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q2(MAXN)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
