      PROGRAM SHYBRID
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER LL
      PARAMETER (LL=8)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LDC)
*     ..
*     .. Scalars in Common ..
      REAL DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER L,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),Q1(LDC),Q2(LDC)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,MU1,MUN,TOL,VA,VB
      INTEGER C,I,MAXIT,N,PRET,STOPT
*     ..
*     .. Local Arrays ..
      REAL B(LDC),SPAR(SPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,GENCOEFS,MATVEC,PIMSCHEBYSHEV,PIMSRGMRESEV,
     +         PIMSSETPAR,PROGRESS,PSSUM,REPORT,SINIT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      L = LL
      N = L*L
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0E-5
      MAXIT = 4*N
      VA = 1.0E-1
      VB = -ACOS(-1.0E0)/6.0E0
      NX = L
      NY = L
      CALL GENCOEFS(1,L,1,L,VA,VB,LDC,COEFS,B)
      CALL SINIT(N,1.0E0,X,1)
      IPAR(1) = LDC
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0E0/COEFS(I,1)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0E0/COEFS(I,1)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0E0/SQRT(COEFS(I,1))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF

      WRITE (6,FMT=9000) '5-point f.-d. PDE','sequential',
     +  ' RGMRESEV and CHEBYSHEV'

* HYBRID
      CALL SINIT(N,0.0E0,X,1)
      ET0 = TIMER()
      DO 60 I = 1,MAXIT
* RGMRESEV
          CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,3,TOL)
          CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                      PSNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 70
          END IF

* CHEBYSHEV
          CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,5,TOL)
          MU1 = SPAR(3)
          MUN = SPAR(4)
          SPAR(3) = 1.0E0 - MUN
          SPAR(4) = 1.0E0 - MU1
          CALL PIMSCHEBYSHEV(X,B,WRK,IPAR,SPAR,MATVEC,DIAGL,DIAGR,PSSUM,
     +                       PSNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 70
          END IF
   60 CONTINUE
   70 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,SPAR,ET,X)
      STOP

 9000 FORMAT ('PIM 2.0',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=8)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER L
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL SMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /PIMA/COEFS
*     ..
      CALL SMVPDE(LDC,IPAR(2),L,COEFS,U,V)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=8)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q1(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=8)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      REAL Q2(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL SCOPY,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL SCOPY(IPAR(4),U,1,V,1)
      CALL SVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
