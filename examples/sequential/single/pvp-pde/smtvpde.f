      SUBROUTINE SMTVPDE(LDC,N,L,COEFS,U,V)
      IMPLICIT NONE

c Coefficient indices: c=1,n=2,s=3,e=4,w=5

C     .. Scalar Arguments ..
      INTEGER L,LDC,N
C     ..
C     .. Array Arguments ..
      REAL COEFS(LDC,*),U(*),V(*)
C     ..
C     .. Local Scalars ..
      INTEGER I
C     ..
      DO 10 I = 1,N
          V(I) = COEFS(I,1)*U(I)
   10 CONTINUE
      DO 20 I = 2,N
          V(I-1) = V(I-1) + COEFS(I,3)*U(I)
   20 CONTINUE
      DO 30 I = 1,N - 1
          V(I+1) = V(I+1) + COEFS(I,2)*U(I)
   30 CONTINUE
      DO 40 I = L + 1,N
          V(I-L) = V(I-L) + COEFS(I,5)*U(I)
   40 CONTINUE
      DO 50 I = 1,N - L
          V(I+L) = V(I+L) + COEFS(I,4)*U(I)
   50 CONTINUE
      RETURN

      END
