      PROGRAM SIDLU
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER LL
      PARAMETER (LL=100)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
*     ..
*     .. Scalars in Common ..
      REAL DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER L,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),LU(LDC,2)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL,VA,VB
      INTEGER C,I,J,MAXIT,N,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      REAL B(LDC),SPAR(SPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,PSSUM,TIMER
      EXTERNAL PSNRM2,PSSUM,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL GENCOEFS,IDLU,MATVEC,PIMSBICG,PIMSBICGSTAB,PIMSCG,
     +         PIMSCGEV,PIMSCGNE,PIMSCGNR,PIMSCGS,PIMSQMR,PIMSRBICGSTAB,
     +         PIMSRGCR,PIMSRGMRES,PIMSRGMRESEV,PIMSSETPAR,PIMSTFQMR,
     +         PROGRESS,REPORT,SINIT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      L = LL
      N = L*L
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = 4*LL
      MAXIT = 200
      VA = 1.0E-1
      VB = -ACOS(-1.0)/6.0
      NX = L
      NY = L
      CALL GENCOEFS(1,L,1,L,VA,VB,LDC,COEFS,B)
      CALL SINIT(N,1.0E0,X,1)
      IPAR(1) = LDC
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)

* Compute sidlu
      DO 10 I = 1,N
          LU(I,1) = COEFS(I,1)
          LU(I,2) = COEFS(I,3)
   10 CONTINUE
      DO 30 I = 1,L
          DO 20 J = (I-1)*L + 2, (I-1)*L + L
              LU(J,2) = LU(J,2)/LU(J-1,1)
              LU(J,1) = LU(J,1) - LU(J,2)*COEFS(J-1,2)
   20     CONTINUE
   30 CONTINUE
      WRITE (6,FMT=9010) '5-point f.-d. PDE','sequential','idlu(0)'
* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCG(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* CGEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGEV(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,4)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGS(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,
     +                   PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGCR(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)
* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,IDLU,IDLU,PSSUM,
     +             PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,IDLU,IDLU,PSSUM,PSNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.0',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning.')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=100)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER L
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL SMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /PIMA/COEFS
*     ..
      CALL SMVPDE(LDC,IPAR(2),L,COEFS,U,V)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=100)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER L
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL SMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /PIMA/COEFS
*     ..
      CALL SMTVPDE(LDC,IPAR(2),L,COEFS,U,V)
      RETURN

      END
      SUBROUTINE IDLU(U,V,IPAR)
      IMPLICIT NONE
*  Solve Ly=u
*     .. Parameters ..
      INTEGER LL
      PARAMETER (LL=100)
      INTEGER LDC
      PARAMETER (LDC=LL*LL)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER L
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),LU(LDC,2)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K
*     ..
*     .. Local Arrays ..
      REAL Y(LDC)
*     ..
*     .. Common blocks ..
      COMMON /B0002/L
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      DO 20 I = 1,L
          K = (I-1)*L + 1
          Y(K) = U(K)
          DO 10 J = K + 1, (I-1)*L + L
              Y(J) = U(J) - LU(J,2)*Y(J-1)
   10     CONTINUE
   20 CONTINUE
*  Solve Uv=y
      DO 40 I = L,1,-1
          K = (I-1)*L + L
          V(K) = Y(K)/LU(K,1)
          DO 30 J = K - 1, (I-1)*L + 1,-1
              V(J) = (Y(J)-COEFS(J,2)*V(J+1))/LU(J,1)
   30     CONTINUE
   40 CONTINUE
      RETURN

      END
