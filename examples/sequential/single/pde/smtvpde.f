      SUBROUTINE SMTVPDE(LDC,LX,LY,COEFS,U,V)
      IMPLICIT NONE
*     .. Scalar Arguments ..
      INTEGER LX,LY,LDC
*     ..
*     .. Array Arguments ..
      REAL COEFS(LDC,*),U(*),V(*)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K,LY1
*     ..
* Matrix-transpose-vector product
* Coefficient indices: c=1,n=2,s=3,e=4,w=5
* Computation is done in j-direction (vertical) and
* then in the i-direction
      LY1 = LY - 1

* 1st column
      V(1) = COEFS(1,1)*U(1) + COEFS(2,3)*U(2) + COEFS(LY+1,5)*U(LY+1)
      DO 10 K = 2,LY1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY)
   10 CONTINUE
      V(LY) = COEFS(LY,1)*U(LY) + COEFS(LY1,2)*U(LY1) + 
     +        COEFS(LY+LY,5)*U(LY+LY)
* Intermediate columns
      K = LY
      DO 30 J = 2,LX - 1
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
          DO 20 I = 2,LY1
              K = K + 1
              V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +               COEFS(K-1,2)*U(K-1) + COEFS(K+LY,5)*U(K+LY) +
     +               COEFS(K-LY,4)*U(K-LY)
   20     CONTINUE
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) +
     +           COEFS(K+LY,5)*U(K+LY) + COEFS(K-LY,4)*U(K-LY)
   30 CONTINUE
* Last column
      K = K + 1
      V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) + 
     +       COEFS(K-LY,4)*U(K-LY)
      DO 40 J = 2,LY1
          K = K + 1
          V(K) = COEFS(K,1)*U(K) + COEFS(K+1,3)*U(K+1) +
     +           COEFS(K-1,2)*U(K-1) + COEFS(K-LY,4)*U(K-LY)
   40 CONTINUE
      K = K + 1
      V(K) = COEFS(K,1)*U(K) + COEFS(K-1,2)*U(K-1) + 
     +       COEFS(K-LY,4)*U(K-LY)
      RETURN

      END
