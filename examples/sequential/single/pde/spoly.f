      PROGRAM SPOLY
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER SPARSIZ
      PARAMETER (SPARSIZ=6)
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      REAL DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,M,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5),GAMMA(NGAMMA),Q1(LDC),Q2(LDC)
*     ..
*     .. Local Scalars ..
      REAL ET,ET0,ET1,TOL,VA,VB
      INTEGER C,I,MAXIT,N,POLYT,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      REAL B(LDC),SPAR(SPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL PSNRM2,TIMER
      EXTERNAL PSNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL GENCOEFS,MATVEC,PIMSBICG,PIMSBICGSTAB,PIMSCG,PIMSCGEV,
     +         PIMSCGNE,PIMSCGNR,PIMSCGS,PIMSQMR,PIMSRBICGSTAB,PIMSRGCR,
     +         PIMSRGMRES,PIMSRGMRESEV,PIMSSETPAR,PIMSTFQMR,POLYL,POLYR,
     +         PROGRESS,PSSUM,REPORT,SINIT,SULSCOEF,SWLSCOEF,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/LX,LY
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0E-5
      MAXIT = INT(N/2)
      MAXIT = 200
      POLYT = 1
      M = 1
      VA = 1.0E-1
      VB = -ACOS(-1.0)/6.0
      NX = LLX
      NY = LLY
      CALL GENCOEFS(1,LX,1,LY,VA,VB,LDC,COEFS,B)
      CALL SINIT(N,1.0E0,X,1)
      IPAR(1) = LDC
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 10 I = 1,N
              Q1(I) = 1.0/COEFS(I,1)
   10     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 20 I = 1,N
              Q2(I) = 1.0/COEFS(I,1)
   20     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0/SQRT(COEFS(I,1))
              Q2(I) = Q1(I)
   30     CONTINUE
      END IF
* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 40 I = 1,M + 1
              GAMMA(I) = 1.0
   40     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL SWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL SULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','sequential',
     +      'Neumann polynomial',M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','sequential',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9010) '5-point f.-d. PDE','sequential',
     +      'unweighted least-squares polynomial',M
      END IF
* CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCG(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,SPAR,ET,X)
* CGEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGEV(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,4)
* Bi-CG
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICG(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,SPAR,ET,X)
* CGS
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGS(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,SPAR,ET,X)
* Bi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,SPAR,ET,X)
* RBi-CGSTAB
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRBICGSTAB(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                   PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,SPAR,ET,X)
* RGMRES
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRES(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,SPAR,ET,X)
* RGMRESEV
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGMRESEV(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,
     +                  PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,SPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (SPAR(I),I=3,6)
* RGCR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSRGCR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,SPAR,ET,X)
* CGNR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,SPAR,ET,X)
* CGNE
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSCGNE(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +              PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,SPAR,ET,X)
* QMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSQMR(X,B,WRK,IPAR,SPAR,MATVEC,TMATVEC,POLYL,POLYR,PSSUM,
     +             PSNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,SPAR,ET,X)
* TFQMR
      CALL PIMSSETPAR(IPAR,SPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL SINIT(IPAR(4),0.0,X,1)
      ET0 = TIMER()
      CALL PIMSTFQMR(X,B,WRK,IPAR,SPAR,MATVEC,POLYL,POLYR,PSSUM,PSNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,SPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (E16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning (degree=',I2,')')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL SMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
*     ..
      CALL SMVPDE(LDC,LX,LY,COEFS,U,V)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      REAL COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL SMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
*     ..
      CALL SMTVPDE(LDC,LX,LY,COEFS,U,V)
      RETURN

      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,M
*     ..
*     .. Arrays in Common ..
      REAL GAMMA(NGAMMA),Q1(LDC)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      REAL T(LDC),W(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL MATVEC,SAXPY,SCOPY,SSCAL,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/LX,LY
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL SCOPY(LOCALN,U,1,T,1)
      CALL SVPROD(LOCALN,Q1,1,T,1)
      CALL SCOPY(LOCALN,T,1,V,1)
      CALL SSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL SVPROD(LOCALN,Q1,1,W,1)
          CALL SAXPY(LOCALN,-1.0,W,1,V,1)
          CALL SAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      REAL U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY,M
*     ..
*     .. Arrays in Common ..
      REAL GAMMA(NGAMMA),Q2(LDC)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      REAL T(LDC),W(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL MATVEC,SAXPY,SCOPY,SSCAL,SVPROD
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /B0002/LX,LY
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL SCOPY(LOCALN,U,1,T,1)
      CALL SVPROD(LOCALN,Q2,1,T,1)
      CALL SCOPY(LOCALN,T,1,V,1)
      CALL SSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL SVPROD(LOCALN,Q2,1,W,1)
          CALL SAXPY(LOCALN,-1.0,W,1,V,1)
          CALL SAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
