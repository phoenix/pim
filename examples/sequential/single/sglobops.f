      SUBROUTINE PSSUM(ISIZE,X,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      INTEGER IPAR(IPARSIZ)
      REAL X(*)
*     ..
      RETURN

      END

      REAL FUNCTION PSNRM2(LOCLEN,U,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      INTEGER IPAR(IPARSIZ)
      REAL U(*)
*     ..
*     .. External Functions ..
      REAL SNRM2
      EXTERNAL SNRM2
*     ..
      PSNRM2 = SNRM2(LOCLEN,U,1)
      RETURN

      END
