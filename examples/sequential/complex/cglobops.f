      SUBROUTINE PCSUM(ISIZE,X,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      COMPLEX X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
      RETURN

      END

      REAL FUNCTION PSCNRM2(LOCLEN,U,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      COMPLEX U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      REAL SCNRM2
      EXTERNAL SCNRM2
*     ..
      PSCNRM2 = SCNRM2(LOCLEN,U,1)
      RETURN

      END
