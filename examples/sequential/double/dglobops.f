      SUBROUTINE PDSUM(ISIZE,X,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER ISIZE
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION X(*)
      INTEGER IPAR(IPARSIZ)
*     ..
      RETURN

      END

      DOUBLE PRECISION FUNCTION PDNRM2(LOCLEN,U,IPAR)
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
*     .. Scalar Arguments ..
      INTEGER LOCLEN
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION DNRM2
      EXTERNAL DNRM2
*     ..
      PDNRM2 = DNRM2(LOCLEN,U,1)
      RETURN

      END
