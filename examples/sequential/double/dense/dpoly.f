      PROGRAM DPOLY
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN),GAMMA(NGAMMA),Q1(LOCLEN),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL
      REAL ET,ET0,ET1
      INTEGER C,I,J,MAXIT,N,POLYT,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LOCLEN),DPAR(DPARSIZ),WRK(LWRK),X(LOCLEN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DINIT,DULSCOEF,DWLSCOEF,MATVEC,PDSUM,PIMDBICG,
     +         PIMDBICGSTAB,PIMDCG,PIMDCGEV,PIMDCGNE,PIMDCGNR,PIMDCGS,
     +         PIMDQMR,PIMDRBICGSTAB,PIMDRGCR,PIMDRGMRES,PIMDRGMRESEV,
     +         PIMDSETPAR,PIMDTFQMR,POLYL,POLYR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      N = LDA
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      POLYT = 1
      M = 1
      DO 20 I = 1,N
          DO 10 J = 1,N
              IF (I.EQ.J) THEN
                  A(I,J) = 4.0D0

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,J) = 1.0D0

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,J) = 1.0D0

              ELSE
                  A(I,J) = 0.0D0
              END IF

   10     CONTINUE
   20 CONTINUE
      CALL DINIT(N,1.0D0,X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0D0/A(I,I)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0D0/A(I,I)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0D0/SQRT(A(I,I))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF
* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 60 I = 1,M + 1
              GAMMA(I) = 1.0D0
   60     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL DWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL DULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9010) 'dense','sequential','Neumann polynomial',
     +      M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9010) 'dense','sequential',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9010) 'dense','sequential',
     +      'unweighted least-squares polynomial',M
      END IF
* CG
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCG(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICG(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGS(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGCR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNE(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)
* QMR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDQMR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning (degree=',I2,')')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL DGEMV('N',IPAR(2),IPAR(2),1.0D0,A,IPAR(1),U,1,0.0D0,V,1)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL DGEMV('T',IPAR(2),IPAR(2),1.0D0,A,IPAR(1),U,1,0.0D0,V,1)
      RETURN

      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q1(LOCLEN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q1,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q1,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q2,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q2,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
