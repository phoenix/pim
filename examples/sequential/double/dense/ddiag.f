      PROGRAM DDIAG
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*

*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LDWRK
      PARAMETER (LDWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN),Q1(LOCLEN),Q2(LOCLEN)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL
      REAL ET,ET0,ET1
      INTEGER C,I,J,MAXIT,N,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LOCLEN),DPAR(DPARSIZ),DWRK(LDWRK),X(LOCLEN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,DINIT,MATVEC,PDSUM,PIMDBICG,PIMDBICGSTAB,
     +         PIMDCG,PIMDCGEV,PIMDCGNE,PIMDCGNR,PIMDCGS,PIMDQMR,
     +         PIMDRBICGSTAB,PIMDRGCR,PIMDRGMRES,PIMDRGMRESEV,
     +         PIMDSETPAR,PIMDTFQMR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      N = LDA
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      DO 20 I = 1,N
          DO 10 J = 1,N
              IF (I.EQ.J) THEN
                  A(I,J) = 4.0D0

              ELSE IF ((I-J).EQ.1) THEN
                  A(I,J) = 1.0D0

              ELSE IF ((J-I).EQ.1) THEN
                  A(I,J) = 1.0D0

              ELSE
                  A(I,J) = 0.0D0
              END IF

   10     CONTINUE
   20 CONTINUE
      CALL DINIT(N,1.0D0,X,1)
      IPAR(1) = LDA
      IPAR(2) = N
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0D0/A(I,I)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0D0/A(I,I)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0D0/SQRT(A(I,I))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF

      WRITE (6,FMT=9010) 'dense','sequential','diagonal'
* CG
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCG(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGEV(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICG(X,B,DWRK,IPAR,DPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGS(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGCR(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNR(X,B,DWRK,IPAR,DPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNE(X,B,DWRK,IPAR,DPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)
* QMR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDQMR(X,B,DWRK,IPAR,DPAR,MATVEC,TMATVEC,DIAGL,DIAGR,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,LDA,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,DWRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning.',/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL DGEMV('N',IPAR(2),IPAR(2),1.0D0,A,IPAR(1),U,1,0.0D0,V,1)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION A(LDA,LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DGEMV
*     ..
*     .. Common blocks ..
      COMMON /PIMA/A
*     ..
      CALL DGEMV('T',IPAR(2),IPAR(2),1.0D0,A,IPAR(1),U,1,0.0D0,V,1)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION Q1(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DCOPY,DVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL DCOPY(IPAR(4),U,1,V,1)
      CALL DVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END

      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LDA
      PARAMETER (LDA=500)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION Q2(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DCOPY,DVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL DCOPY(IPAR(4),U,1,V,1)
      CALL DVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
