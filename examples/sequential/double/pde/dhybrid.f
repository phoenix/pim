      PROGRAM DHYBRID
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER LWRK
      PARAMETER (LWRK= (4+BASIS)*LDC)
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),Q1(LDC),Q2(LDC)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION MU1,MUN,TOL,VA,VB
      REAL ET,ET0,ET1
      INTEGER C,I,MAXIT,N,PRET,STOPT
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LDC),DPAR(DPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DIAGL,DIAGR,DINIT,GENCOEFS,MATVEC,PDSUM,PIMDCHEBYSHEV,
     +         PIMDRGMRESEV,PIMDSETPAR,PROGRESS,REPORT
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      PRET = 1
      STOPT = 1
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      VA = 1.0D-1
      VB = -ACOS(-1.0D0)/6.0D0
      NX = LLX
      NY = LLY
      CALL GENCOEFS(1,LX,1,LY,VA,VB,LDC,COEFS,B)

      CALL DINIT(N,1.0D0,X,1)
      CALL MATVEC(X,B,IPAR)

* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 30 I = 1,N
              Q1(I) = 1.0D0/COEFS(I,1)
   30     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 40 I = 1,N
              Q2(I) = 1.0D0/COEFS(I,1)
   40     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 50 I = 1,N
              Q1(I) = 1.0D0/SQRT(COEFS(I,1))
              Q2(I) = Q1(I)
   50     CONTINUE
      END IF

      WRITE (6,FMT=9000) '5-point f.-d. PDE','sequential',
     +  ' RGMRESEV and CHEBYSHEV'
* HYBRID
      CALL DINIT(N,0.0D0,X,1)
      ET0 = TIMER()
      DO 60 I = 1,MAXIT
* RGMRESEV
          CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,3,TOL)
          CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                      PDNRM2,PROGRESS)
          IF (IPAR(12).NE.-1) THEN
              IPAR(11) = I
              GO TO 70
          END IF

* CHEBYSHEV
          CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,5,TOL)
          MU1 = DPAR(3)
          MUN = DPAR(4)
          DPAR(3) = 1.0D0 - MUN
          DPAR(4) = 1.0D0 - MU1
          CALL PIMDCHEBYSHEV(X,B,WRK,IPAR,DPAR,MATVEC,DIAGL,DIAGR,PDSUM,
     +                       PDNRM2,PROGRESS)
          IF ((IPAR(12).EQ.0) .OR. (IPAR(12).EQ.-6) .OR.
     +        (IPAR(12).EQ.-7)) THEN
              IPAR(11) = I
              GO TO 70
          END IF
   60 CONTINUE
   70 CONTINUE
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('HYBRID',IPAR,DPAR,ET,X)

      STOP

 9000 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,/)
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL DMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
*     ..
      CALL DMVPDE(LDC,LX,LY,COEFS,U,V)
      RETURN

      END
      SUBROUTINE DIAGL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION Q1(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL DCOPY,DVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ1/Q1
*     ..
      CALL DCOPY(IPAR(4),U,1,V,1)
      CALL DVPROD(IPAR(4),Q1,1,V,1)
      RETURN

      END
      SUBROUTINE DIAGR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION Q2(LDC)
*     ..
*     .. External Subroutines ..
      EXTERNAL DCOPY,DVPROD
*     ..
*     .. Common blocks ..
      COMMON /PIMQ2/Q2
*     ..
      CALL DCOPY(IPAR(4),U,1,V,1)
      CALL DVPROD(IPAR(4),Q2,1,V,1)
      RETURN

      END
