      PROGRAM DIDLU
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LDC+2*BASIS)
*     ..
*     .. Scalars in Common ..
      DOUBLE PRECISION DHX,DHX2,DHXY2,DHY,DHY2
      INTEGER LX,LY,NX,NX1,NY,NY1
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),LU(LDC,2)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL,VA,VB
      REAL ET,ET0,ET1
      INTEGER C,I,J,MAXIT,N,PRET,STOPT,V
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(LDC),DPAR(DPARSIZ),WRK(LWRK),X(LDC)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DINIT,GENCOEFS,IDLU,MATVEC,PDSUM,PIMDBICG,PIMDBICGSTAB,
     +         PIMDCG,PIMDCGEV,PIMDCGNE,PIMDCGNR,PIMDCGS,PIMDQMR,
     +         PIMDRBICGSTAB,PIMDRGCR,PIMDRGMRES,PIMDRGMRESEV,
     +         PIMDSETPAR,PIMDTFQMR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC ACOS
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /DPVAR/DHX,DHY,DHX2,DHY2,DHXY2
      COMMON /INTVAR/NX,NY,NX1,NY1
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      LX = LLX
      LY = LLY
      N = LX*LY
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(N/2)
      VA = 1.0D-1
      VB = -ACOS(-1.0D0)/6.0D0
      NX = LLX
      NY = LLY
      CALL GENCOEFS(1,LX,1,LY,VA,VB,LDC,COEFS,B)
      CALL DINIT(N,1.0D0,X,1)
      CALL MATVEC(X,B,IPAR)

* Compute didlu
      DO 10 I = 1,N
          LU(I,1) = COEFS(I,1)
          LU(I,2) = COEFS(I,3)
   10 CONTINUE
      DO 30 I = 1,LX
          DO 20 J = (I-1)*LY + 2, (I-1)*LY + LY
              LU(J,2) = LU(J,2)/LU(J-1,1)
              LU(J,1) = LU(J,1) - LU(J,2)*COEFS(J-1,2)
   20     CONTINUE
   30 CONTINUE
      WRITE (6,FMT=9010) '5-point f.-d. PDE','sequential','idlu(0)'
* CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCG(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICG(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGS(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,V,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +                  PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGCR(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNE(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)
* QMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDQMR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,IDLU,IDLU,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,LDC,N,N,N,C,-1,-1,PRET,STOPT,MAXIT,TOL)
      CALL DINIT(IPAR(4),0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,WRK,IPAR,DPAR,MATVEC,IDLU,IDLU,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)
      STOP


 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT ('PIM 2.3',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning.')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL DMVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
*     ..
      CALL DMVPDE(LDC,LX,LY,COEFS,U,V)
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5)
*     ..
*     .. External Subroutines ..
      EXTERNAL DMTVPDE
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
*     ..
      CALL DMTVPDE(LDC,LX,LY,COEFS,U,V)
      RETURN

      END
      SUBROUTINE IDLU(U,V,IPAR)
      IMPLICIT NONE
*  Solve Ly=u
*     .. Parameters ..
      INTEGER LLX
      PARAMETER (LLX=200)
      INTEGER LLY
      PARAMETER (LLY=200)
      INTEGER LDC
      PARAMETER (LDC=LLX*LLY)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER LX,LY
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION COEFS(LDC,5),LU(LDC,2)
*     ..
*     .. Local Scalars ..
      INTEGER I,J,K
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION Y(LDC)
*     ..
*     .. Common blocks ..
      COMMON /B0002/LX,LY
      COMMON /PIMA/COEFS
      COMMON /PIMQ1/LU
*     ..
      DO 20 I = 1,LX
          K = (I-1)*LY + 1
          Y(K) = U(K)
          DO 10 J = K + 1, (I-1)*LY + LY
              Y(J) = U(J) - LU(J,2)*Y(J-1)
   10     CONTINUE
   20 CONTINUE
*  Solve Uv=y
      DO 40 I = LX,1,-1
          K = (I-1)*LY + LY
          V(K) = Y(K)/LU(K,1)
          DO 30 J = K - 1, (I-1)*LY + 1,-1
              V(J) = (Y(J)-COEFS(J,2)*V(J+1))/LU(J,1)
   30     CONTINUE
   40 CONTINUE
      RETURN

      END
