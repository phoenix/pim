      PROGRAM DPOLY
      IMPLICIT NONE
*           PIM -- The Parallel Iterative Methods package
*           ---------------------------------------------
*
*                      Rudnei Dias da Cunha
*     National Supercomputing Centre and Mathematics Institute
*         Universidade Federal do Rio Grande do Sul, Brasil
*
*                          Tim Hopkins
*     Computing Laboratory, University of Kent at Canterbury, U.K.
*
* ----------------------------------------------------------------------
*
* Reads a file in Harwell standard format for sparse matrices and
* solves the system with the PIM routines.
*
* The system solved has a rhs set such that the solution is the
*
* Code based on the sample given in pp. 14-16, Duff, I.S., Grimes,
* R.G. and Lewis, J.G., "Users' Guide for the Harwell-Boeing Sparse
* Matrix Collection (Release I)", Report TR/PA/92/86, CERFACS.
*  Set name of Harwell data file
*     .. Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER INUNIT
      PARAMETER (INUNIT=1)
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER MAXN1
      PARAMETER (MAXN1=MAXN+1)
      INTEGER MAXNZ
      PARAMETER (MAXNZ=347)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=MAXN)
      INTEGER BASIS
      PARAMETER (BASIS=10)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q1(MAXN),Q2(MAXN),VALUES(MAXNZ)
      INTEGER POINTR(MAXN1),ROWIND(MAXNZ)
*     ..
*     .. Local Scalars ..
      DOUBLE PRECISION TOL
      REAL ET,ET0,ET1
      INTEGER C,I,INDCRD,J,MAXIT,NCOL,NELTVL,NNZERO,NRHS,NROW,NRSHIX,
     +        POLYT,PRET,PTRCRD,RHSCRD,STOPT,TOTCRD,V,VALCRD
      LOGICAL FEX
      CHARACTER*3 MXTYPE,RHSTYP
      CHARACTER*8 KEY
      CHARACTER*16 INDFMT,PTRFMT
      CHARACTER*20 RHSFMT,VALFMT
      CHARACTER*72 TITLE
      CHARACTER*80 INFIL
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION B(MAXN),DPAR(DPARSIZ),WRK(LWRK),X(MAXN)
      INTEGER IPAR(IPARSIZ)
*     ..
*     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      REAL TIMER
      EXTERNAL PDNRM2,TIMER
*     ..
*     .. External Subroutines ..
      EXTERNAL DINIT,DULSCOEF,DWLSCOEF,MATVEC,PDSUM,PIMDBICG,
     +         PIMDBICGSTAB,PIMDCG,PIMDCGEV,PIMDCGNE,PIMDCGNR,PIMDCGS,
     +         PIMDQMR,PIMDRBICGSTAB,PIMDRGCR,PIMDRGMRES,PIMDRGMRESEV,
     +         PIMDSETPAR,PIMDTFQMR,POLYL,POLYR,PROGRESS,REPORT,TMATVEC
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC INT,SQRT
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMA/VALUES
      COMMON /PIMAA/POINTR,ROWIND
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
*     ..
      INFIL = 'nos4'
*  Open the file infil
      INQUIRE (FILE=INFIL,EXIST=FEX)
      IF (.NOT.FEX) THEN
          PRINT *,'Input file not found. Exiting.'
          STOP

      END IF

      OPEN (INUNIT,FILE=INFIL,STATUS='old')
*  Read in header block
      READ (INUNIT,FMT=9010) TITLE,KEY,TOTCRD,PTRCRD,INDCRD,VALCRD,
     +  RHSCRD,MXTYPE,NROW,NCOL,NNZERO,NELTVL,PTRFMT,INDFMT,VALFMT,
     +  RHSFMT
      IF (RHSCRD.GT.0) THEN
          READ (INUNIT,FMT=9020) RHSTYP,NRHS,NRSHIX
      END IF
*  Read matrix structure
      READ (INUNIT,FMT=PTRFMT) (POINTR(I),I=1,NCOL+1)
      READ (INUNIT,FMT=INDFMT) (ROWIND(I),I=1,NNZERO)
      IF (VALCRD.GT.0) THEN
*  Read matrix values
          IF (MXTYPE(3:3).EQ.'A') THEN
              READ (INUNIT,FMT=VALFMT) (VALUES(I),I=1,NNZERO)

          ELSE
              PRINT *,'Matrix not in standard sparse format. Exiting.'
              CLOSE (INUNIT)
              STOP

          END IF

      ELSE
*  Just a pattern for the matrix, generate values
          DO 20 J = 1,NROW
              DO 10 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) THEN
                      VALUES(I) = 4.0D0

                  ELSE
                      VALUES(I) = -1.0D0
                  END IF

   10         CONTINUE
   20     CONTINUE
      END IF

      CLOSE (INUNIT)
      C = BASIS
      V = BASIS1
      PRET = 1
      STOPT = 5
      TOL = 1.0D-10
      MAXIT = INT(NROW/2)
      POLYT = 2
      M = 1
      CALL DINIT(NROW,1.0D0,X,1)
      IPAR(1) = MAXN
      IPAR(2) = NROW
      CALL MATVEC(X,B,IPAR)
* Compute preconditioners
      IF (PRET.EQ.1) THEN
          DO 40 J = 1,NROW
              DO 30 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) Q1(J) = 1.0D0/VALUES(I)
   30         CONTINUE
   40     CONTINUE

      ELSE IF (PRET.EQ.2) THEN
          DO 60 J = 1,NROW
              DO 50 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) Q2(J) = 1.0D0/VALUES(I)
   50         CONTINUE
   60     CONTINUE

      ELSE IF (PRET.EQ.3) THEN
          DO 80 J = 1,NROW
              DO 70 I = POINTR(J),POINTR(J+1) - 1
                  IF (ROWIND(I).EQ.J) THEN
                      Q1(J) = 1.0D0/SQRT(VALUES(I))
                      Q2(J) = Q1(J)
                  END IF

   70         CONTINUE
   80     CONTINUE
      END IF
* Set polynomial preconditioner coefficients
      IF (POLYT.EQ.1) THEN
          DO 90 I = 1,M + 1
              GAMMA(I) = 1.0D0
   90     CONTINUE

      ELSE IF (POLYT.EQ.2) THEN
          CALL DWLSCOEF(M,GAMMA)

      ELSE IF (POLYT.EQ.3) THEN
          CALL DULSCOEF(M,GAMMA)
      END IF

      IF (POLYT.EQ.1) THEN
          WRITE (6,FMT=9030) 'dense','sequential','Neumann polynomial',
     +      M

      ELSE IF (POLYT.EQ.2) THEN
          WRITE (6,FMT=9030) 'dense','sequential',
     +      'weighted least-squares polynomial',M

      ELSE IF (POLYT.EQ.3) THEN
          WRITE (6,FMT=9030) 'dense','sequential',
     +      'unweighted least-squares polynomial',M
      END IF
* CG
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCG(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +            PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CG',IPAR,DPAR,ET,X)
* CGEV
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,4)
* Bi-CG
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICG(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CG',IPAR,DPAR,ET,X)
* CGS
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGS(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +             PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGS',IPAR,DPAR,ET,X)
* Bi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('Bi-CGSTAB',IPAR,DPAR,ET,X)
* RBi-CGSTAB
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,V,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRBICGSTAB(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                   PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RBi-CGSTAB',IPAR,DPAR,ET,X)
* RGMRES
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRES(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +                PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRES',IPAR,DPAR,ET,X)
* RGMRESEV
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGMRESEV(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,
     +                  PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGMRESEV',IPAR,DPAR,ET,X)
      WRITE (6,FMT=9000) 'EIGENVALUES REGION', (DPAR(I),I=3,6)
* RGCR
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDRGCR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +              PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('RGCR',IPAR,DPAR,ET,X)
* CGNR
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNR',IPAR,DPAR,ET,X)
* CGNE
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDCGNE(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +              PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('CGNE',IPAR,DPAR,ET,X)
* QMR
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDQMR(X,B,WRK,IPAR,DPAR,MATVEC,TMATVEC,POLYL,POLYR,PDSUM,
     +             PDNRM2,PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('QMR',IPAR,DPAR,ET,X)
* TFQMR
      CALL PIMDSETPAR(IPAR,DPAR,MAXN,NROW,NROW,NROW,C,-1,-1,PRET,STOPT,
     +                MAXIT,TOL)
      CALL DINIT(NROW,0.0D0,X,1)
      ET0 = TIMER()
      CALL PIMDTFQMR(X,B,WRK,IPAR,DPAR,MATVEC,POLYL,POLYR,PDSUM,PDNRM2,
     +               PROGRESS)
      ET1 = TIMER()
      ET = ET1 - ET0
      CALL REPORT('TFQMR',IPAR,DPAR,ET,X)
      STOP

 9000 FORMAT (A,/,4 (D16.10,1X))
 9010 FORMAT (A72,A8,/,5I14,/,A3,11X,4I14,/,2A16,2A20)
 9020 FORMAT (A3,11X,2I14)
 9030 FORMAT ('PIM 2.2',/,'Test program for ',A,' data in ',A,' mode.',
     +       /,'Using ',A,' preconditioning (degree=',I2,')')
      END
      SUBROUTINE MATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER MAXN1
      PARAMETER (MAXN1=MAXN+1)
      INTEGER MAXNZ
      PARAMETER (MAXNZ=347)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION VALUES(MAXNZ)
      INTEGER POINTR(MAXN1),ROWIND(MAXNZ)
*     ..
*     .. Local Scalars ..
      INTEGER I,II,J,N
*     ..
*     .. External Subroutines ..
      EXTERNAL DINIT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/VALUES
      COMMON /PIMAA/POINTR,ROWIND
*     ..
      N = IPAR(2)
      CALL DINIT(N,0.0D0,V,1)
      DO 20 J = 1,N
          DO 10 I = POINTR(J),POINTR(J+1) - 1
              II = ROWIND(I)
              V(II) = V(II) + U(J)*VALUES(I)
   10     CONTINUE
   20 CONTINUE
      RETURN

      END
      SUBROUTINE TMATVEC(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER MAXN1
      PARAMETER (MAXN1=MAXN+1)
      INTEGER MAXNZ
      PARAMETER (MAXNZ=347)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION VALUES(MAXNZ)
      INTEGER POINTR(MAXN1),ROWIND(MAXNZ)
*     ..
*     .. Local Scalars ..
      INTEGER I,II,J,N
*     ..
*     .. External Subroutines ..
      EXTERNAL DINIT
*     ..
*     .. Common blocks ..
      COMMON /PIMA/VALUES
      COMMON /PIMAA/POINTR,ROWIND
*     ..
      N = IPAR(2)
      CALL DINIT(N,0.0D0,V,1)
      DO 20 J = 1,N
          DO 10 I = POINTR(J),POINTR(J+1) - 1
              II = ROWIND(I)
              V(J) = V(J) + U(II)*VALUES(I)
   10     CONTINUE
   20 CONTINUE
      RETURN

      END
      SUBROUTINE POLYL(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=MAXN)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q1(MAXN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ1/Q1
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q1,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q1,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
      SUBROUTINE POLYR(U,V,IPAR)
      IMPLICIT NONE
*     .. Parameters ..
      INTEGER MAXN
      PARAMETER (MAXN=100)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=MAXN)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
*     ..
*     .. Array Arguments ..
      DOUBLE PRECISION U(*),V(*)
      INTEGER IPAR(*)
*     ..
*     .. Scalars in Common ..
      INTEGER M
*     ..
*     .. Arrays in Common ..
      DOUBLE PRECISION GAMMA(NGAMMA),Q2(MAXN)
*     ..
*     .. Local Scalars ..
      INTEGER I,LOCALN
*     ..
*     .. Local Arrays ..
      DOUBLE PRECISION T(LOCLEN),W(LOCLEN)
*     ..
*     .. External Subroutines ..
      EXTERNAL DAXPY,DCOPY,DSCAL,DVPROD,MATVEC
*     ..
*     .. Common blocks ..
      COMMON /B0001/GAMMA,M
      COMMON /PIMQ2/Q2
*     ..
      LOCALN = IPAR(4)
      CALL DCOPY(LOCALN,U,1,T,1)
      CALL DVPROD(LOCALN,Q2,1,T,1)
      CALL DCOPY(LOCALN,T,1,V,1)
      CALL DSCAL(LOCALN,GAMMA(M+1),V,1)
      DO 10 I = 1,M
          CALL MATVEC(V,W,IPAR)
          CALL DVPROD(LOCALN,Q2,1,W,1)
          CALL DAXPY(LOCALN,-1.0D0,W,1,V,1)
          CALL DAXPY(LOCALN,GAMMA(M-I+1),T,1,V,1)
   10 CONTINUE
      RETURN

      END
