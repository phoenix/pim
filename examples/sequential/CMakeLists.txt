if (PIM_DATATYPE STREQUAL "single")
    add_subdirectory(single)
elseif (PIM_DATATYPE STREQUAL "double")
    add_subdirectory(double)
elseif (PIM_DATATYPE STREQUAL "complex")
    add_subdirectory(complex)
elseif (PIM_DATATYPE STREQUAL "dcomplex")
    add_subdirectory(dcomplex)
else()
    message(FATAL_ERROR "PIM_DATATYPE not recognised. Was" ${PIM_DATATYPE})
endif()