#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "PIM::PIM-Library" for configuration "Debug"
set_property(TARGET PIM::PIM-Library APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(PIM::PIM-Library PROPERTIES
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib64/libPIM-Library_d.so"
  IMPORTED_SONAME_DEBUG "libPIM-Library_d.so"
  )

list(APPEND _cmake_import_check_targets PIM::PIM-Library )
list(APPEND _cmake_import_check_files_for_PIM::PIM-Library "${_IMPORT_PREFIX}/lib64/libPIM-Library_d.so" )

# Import target "PIM::PIM-LibraryCommon" for configuration "Debug"
set_property(TARGET PIM::PIM-LibraryCommon APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(PIM::PIM-LibraryCommon PROPERTIES
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib64/libPIM-LibraryCommon_d.so"
  IMPORTED_SONAME_DEBUG "libPIM-LibraryCommon_d.so"
  )

list(APPEND _cmake_import_check_targets PIM::PIM-LibraryCommon )
list(APPEND _cmake_import_check_files_for_PIM::PIM-LibraryCommon "${_IMPORT_PREFIX}/lib64/libPIM-LibraryCommon_d.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
