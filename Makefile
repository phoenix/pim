#----------------------------------------------------------------------
#	  PIM -- The Parallel Iterative Methods package
#	  ---------------------------------------------
#
#		     Rudnei Dias da Cunha
#     National Supercomputing Centre and Mathematics Institute
#	 Universidade Federal do Rio Grande do Sul, Brasil
#
#			 Tim Hopkins
#    Computing Laboratory, University of Kent at Canterbury, U.K.
#
#----------------------------------------------------------------------
#
#  Change the variables below as needed; refer to the User's Guide
#  installation notes for more information.
#	HOME	  your top directory, e.g. /u1/users/fred
#	FC   	  your Fortran compiler of choice.
#	FFLAGS	  flags for the Fortran compilation of main programs
#		 (example programs)
#	OFFLAGS	  flags for the Fortran compilation of separate
#		  modules (PIM routines and modules of examples);
#		  MUST contain at least flag for separate compilation
#		  (usually -c)
#	AR	  the archiver program (usually ar).
#	HASRANLIB either t (true) or f (false), indicating if it is
#		  necessary to use a random library program (usually
#		  ranlib) to build the PIM library
#	BLASLIB	  the command switches for the library containing the
#		  BLAS routines; for instance, if a library libblas.a
#		  is installed in a system-wide basis, then set BLASLIB
#		  to -lblas
#	LAPACKLIB The command switches for the library containing the
#		  xHSEQR LAPACK routine (and associated routines); for
#		  instance, if a library liblapack.a is present in your
#		  system, then set LAPACKLIB to -llapack
#	PARLIB	  The compilation switches for the parallel libraries.
#		  This variable should be left blank if PIM is to be
#		  used in sequential mode. For example, using PVM with
#		 properly set variables PVM_ROOT and PVM_ARCH, the
#		 variable PARLIB can be defined as
#		 -L$(PVM_ROOT)/lib/$(PVM_ARCH) -lfpvm3 -lpvm3 -lgpvm3
#
#FC	= mpiifort
#FFLAGS	= -g -C -O3 -ipo
#OFFLAGS	= -g -C -c -O3 -ipo
#AR	= ar
#HASRANLIB	= f

#BLASLIB	= -qopenmp -L${MKLROOT}/lib/intel64 -Wl,-rpath,${MKLROOT}/lib/intel64  -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lpthread -lm
#LAPACKLIB = ${BLASLIB}

#PARLIB	= -L$(PVM_ROOT)/lib/$(PVM_ARCH) -lfpvm3 -lpvm3 -lgpvm3
#PARLIB	=

#
# Do not modify below this line
#
VERSION	= 2
REV	= 3
PIMDIR	= lib/pim$(VERSION)$(REV)
COMMON	= $(HOME)/$(PIMDIR)/common
SP	= $(HOME)/$(PIMDIR)/single
SPLIB	= $(SP)/lib
SPSRC	= $(SP)/src
DP	= $(HOME)/$(PIMDIR)/double
DPLIB	= $(DP)/lib
DPSRC	= $(DP)/src
CP	= $(HOME)/$(PIMDIR)/complex
CPLIB	= $(CP)/lib
CPSRC	= $(CP)/src
ZP	= $(HOME)/$(PIMDIR)/dcomplex
ZPLIB	= $(ZP)/lib
ZPSRC	= $(ZP)/src
EXMPLDIR= $(HOME)/$(PIMDIR)/examples

PIMDIRS	= $(SPLIB) $(SPSRC) $(DPLIB) $(DPSRC) $(CPLIB) $(CPSRC) $(ZPLIB) $(ZPSRC)
SEQDIR	= sequential/single/dense sequential/single/pde \
	  sequential/single/pvp-pde \
	  sequential/single/harwell-boeing \
	  sequential/double/dense sequential/double/pde \
	  sequential/double/harwell-boeing \
	  sequential/complex/dense \
	  sequential/dcomplex/dense

PVMDIR	= pvm/single/dense pvm/single/pde \
	  pvm/double/dense pvm/double/pde \
	  pvm/complex/dense \
	  pvm/dcomplex/dense

MPIDIR	= mpi/single/dense mpi/single/pde \
	  mpi/double/dense mpi/double/pde \
	  mpi/complex/dense \
	  mpi/dcomplex/dense

EXDIRS	= $(SEQDIR) $(PVMDIR) $(MPIDIR)

SHELL	= /bin/sh

PRECISION	= single double complex dcomplex

INCFILE	=

MPIINCLUDE	= mpif.h
PVMINCLUDE	= fpvm3.h

#
# Makefiles
#
makefiles:
	@echo
	@echo "Making makefiles... "
#   PIM libraries and sources
	@(echo "HOME=$(HOME)"; echo "PIMDIR=$(PIMDIR)"; echo "FC=$(FC)"; echo "FFLAGS=$(FFLAGS)"; echo "OFFLAGS=$(OFFLAGS)"; echo "AR=$(AR)"; echo "HASRANLIB=$(HASRANLIB)"; echo "SHELL=$(SHELL)") > vars
	@for dir in $(PIMDIRS) ;\
	do\
		cat vars $$dir/Makefile.orig > $$dir/Makefile ;\
	done
#   Examples
	@(echo "HOME=$(HOME)"; echo "PIMDIR=$(PIMDIR)"; echo "FC=$(FC)"; echo "FFLAGS=$(FFLAGS)"; echo "OFFLAGS=$(OFFLAGS)"; echo "AR=$(AR)"; echo "BLASLIB=$(BLASLIB)"; echo "LAPACKLIB=$(LAPACKLIB)"; echo "PARLIB=$(PARLIB)"; echo "SHELL=$(SHELL)") > vars
	@for dir in $(EXDIRS) ;\
	do\
		cat vars $(EXMPLDIR)/$$dir/Makefile.orig > $(EXMPLDIR)/$$dir/Makefile ;\
	done
	@cat vars $(COMMON)/Makefile.orig > $(COMMON)/Makefile
	@rm -f vars
	@echo "Done."

#
# Single-precision machine-constants
#
smachcons: $(COMMON)/compute-smachcons.f
	@echo
	@echo "Using LAPACK SLAMCH to compute machine constants."
	@if test -f $(COMMON)/Makefile ; then\
		cd $(COMMON); make smachcons; \
		echo "Done.";\
	else\
		echo;\
		echo "Error: no Makefile found on $(COMMON)";\
	fi;

#
# Double-precision machine-constants
#
dmachcons: $(COMMON)/compute-dmachcons.f
	@echo
	@echo "Using LAPACK DLAMCH to compute machine constants."
	@if test -f $(COMMON)/Makefile ; then\
		cd $(COMMON); make dmachcons; \
		echo "Done.";\
	else\
		echo;\
		echo "Error: no Makefile found on $(COMMON)";\
	fi;

#
# Cleaning machine-constants
#
clean-machcons:
	@echo
	@echo "Removing machine constants..."
	rm -f $(COMMON)/?machcons.f $(COMMON)/compute-?machcons
	@echo "Done."

#
# Single-precision PIM
#
single: $(SPLIB)/libpim.a $(SPSRC)/*.o
	@echo "Done."
$(SPLIB)/libpim.a:
	@echo
	@echo "Making single-precision PIM library"
	@if test -f $(SPLIB)/Makefile ; then\
		if test ! -f $(COMMON)/smachcons.f ; then\
			echo;\
 			echo "Using IEEE-754 machine constants...";\
			echo;\
 			cp $(COMMON)/smachcons.f.ieee754 $(COMMON)/smachcons.f;\
			cd $(SPLIB); make all;\
			rm $(COMMON)/smachcons.f;\
		else\
			echo;\
 			echo "Using machine constants on $(COMMON)/smachcons.f...";\
			echo;\
			cd $(SPLIB); make all;\
		fi;\
	else\
		echo;\
		echo "Error: no Makefile found on $(SPLIB)";\
	fi;
$(SPSRC)/*.o:
	@echo
	@echo "Making single-precision PIM iterative routines"
	@if test -f $(SPSRC)/Makefile ; then\
		cd $(SPSRC); make all;\
	else\
		echo;\
		echo "Error: no Makefile found on $(SPSRC)";\
	fi;

#
# Double-precision PIM
#
double: $(DPLIB)/libpim.a $(DPSRC)/*.o
	@echo "Done."
$(DPLIB)/libpim.a:
	@echo
	@echo "Making double-precision PIM library"
	@if test -f $(DPLIB)/Makefile ; then\
		if test ! -f $(COMMON)/dmachcons.f ; then\
			echo;\
 			echo "Using IEEE-754 machine constants...";\
			echo;\
 			cp $(COMMON)/dmachcons.f.ieee754 $(COMMON)/dmachcons.f;\
			cd $(DPLIB); make all;\
			rm -f $(COMMON)/dmachcons.f;\
		else\
			echo;\
 			echo "Using machine constants on $(COMMON)/dmachcons.f...";\
			echo;\
			cd $(DPLIB); make all;\
		fi;\
	else\
		echo;\
		echo "Error: no Makefile found on $(DPLIB)";\
	fi;
$(DPSRC)/*.o:
	@echo
	@echo "Making double-precision PIM iterative routines"
	@if test -f $(DPSRC)/Makefile ; then\
		cd $(DPSRC); make all;\
	else\
		echo;\
		echo "Error: no Makefile found on $(DPSRC)";\
	fi;

#
# Complex PIM
#
complex: $(CPLIB)/libpim.a $(CPSRC)/*.o
	@echo "Done."
$(CPLIB)/libpim.a:
	@echo
	@echo "Making complex PIM library"
	@if test -f $(CPLIB)/Makefile ; then\
		if test ! -f $(COMMON)/smachcons.f ; then\
			echo;\
 			echo "Using IEEE-754 machine constants...";\
			echo;\
 			cp $(COMMON)/smachcons.f.ieee754 $(COMMON)/smachcons.f;\
			cd $(CPLIB); make all;\
 			rm $(COMMON)/smachcons.f;\
		else\
			echo;\
 			echo "Using machine constants on $(COMMON)/smachcons.f...";\
			echo;\
			cd $(CPLIB); make all;\
		fi;\
	else\
		echo;\
		echo "Error: no Makefile found on $(CPLIB)";\
	fi;
$(CPSRC)/*.o:
	@echo
	@echo "Making complex PIM iterative routines"
	@if test -f $(CPSRC)/Makefile ; then\
		cd $(CPSRC); make all;\
	else\
		echo;\
		echo "Error: no Makefile found on $(CPSRC)";\
	fi;

#
# Double complex PIM
#
dcomplex: $(ZPLIB)/libpim.a $(ZPSRC)/*.o
	@echo "Done."
$(ZPLIB)/libpim.a:
	@echo
	@echo "Making double complex PIM library"
	@if test -f $(ZPLIB)/Makefile ; then\
		if test ! -f $(COMMON)/dmachcons.f ; then\
			echo;\
 			echo "Using IEEE-754 machine constants...";\
			echo;\
 			cp $(COMMON)/dmachcons.f.ieee754 $(COMMON)/dmachcons.f;\
			cd $(ZPLIB); make all;\
 			rm $(COMMON)/dmachcons.f;\
		else\
			echo;\
 			echo "Using machine constants on $(COMMON)/dmachcons.f...";\
			echo;\
			cd $(ZPLIB); make all;\
		fi;\
	else\
		echo;\
		echo "Error: no Makefile found on $(ZPLIB)";\
	fi;
$(ZPSRC)/*.o:
	@echo
	@echo "Making double complex PIM iterative routines"
	@if test -f $(ZPSRC)/Makefile ; then\
		cd $(ZPSRC); make all;\
	else\
		echo;\
		echo "Error: no Makefile found on $(ZPSRC)";\
	fi;

#
# Examples: Default make
#
default:
	@if test -f $(EXMPLDIR)/$(WHICHDIR)/Makefile ; then\
		cd $(EXMPLDIR)/$(WHICHDIR); make all;\
	else\
		echo;\
		echo "Error: no Makefile found on $(WHICHDIR)";\
	fi;

#
# Examples: Sequential
#
sequential/single/dense: single
	@echo
	@echo "Making examples/sequential/single/dense"
	@make default WHICHDIR=sequential/single/dense

sequential/single/pde: single
	@echo
	@echo "Making examples/sequential/single/pde"
	@make default WHICHDIR=/sequential/single/pde

sequential/single/pvp-pde: single
	@echo
	@echo "Making examples/sequential/single/pvp-pde"
	@make default WHICHDIR=/sequential/single/pvp-pde

sequential/single/harwell-boeing: single
	@echo
	@echo "Making examples/sequential/single/harwell-boeing"
	@make default WHICHDIR=/sequential/single/harwell-boeing

sequential/double/dense: double
	@echo
	@echo "Making examples/sequential/double/dense"
	@make default WHICHDIR=/sequential/double/dense

sequential/double/pde: double
	@echo
	@echo "Making examples/sequential/double/pde"
	@make default WHICHDIR=/sequential/double/pde

sequential/double/harwell-boeing: double
	@echo
	@echo "Making examples/sequential/double/harwell-boeing"
	@make default WHICHDIR=/sequential/double/harwell-boeing

sequential/complex/dense: complex
	@echo
	@echo "Making examples/sequential/complex/dense"
	@make default WHICHDIR=/sequential/complex/dense

sequential/dcomplex/dense: dcomplex
	@echo
	@echo "Making examples/sequential/dcomplex/dense"
	@make default WHICHDIR=/sequential/dcomplex/dense

allsequential:
	make sequential/single/dense
	make sequential/single/pde
	make sequential/single/harwell-boeing
	make sequential/double/dense
	make sequential/double/pde
	make sequential/double/harwell-boeing
	make sequential/complex/dense
	make sequential/dcomplex/dense

#
# Examples: PVM
#
pvm/single/dense: single
	@echo
	@echo "Making examples/pvm/single/dense"
	@make default WHICHDIR=/pvm/single/dense

pvm/single/pde: single
	@echo
	@echo "Making examples/pvm/single/pde"
	@make default WHICHDIR=/pvm/single/pde

pvm/double/dense: double
	@echo
	@echo "Making examples/pvm/double/dense"
	@make default WHICHDIR=/pvm/double/dense

pvm/double/pde: double
	@echo
	@echo "Making examples/pvm/double/pde"
	@make default WHICHDIR=/pvm/double/pde

pvm/complex/dense: complex
	@echo
	@echo "Making examples/pvm/complex/dense"
	@make default WHICHDIR=/pvm/complex/dense

pvm/dcomplex/dense: dcomplex
	@echo
	@echo "Making examples/pvm/dcomplex/dense"
	@make default WHICHDIR=/pvm/dcomplex/dense

allpvm:
	make pvm/single/dense
	make pvm/single/pde
	make pvm/double/dense
	make pvm/double/pde
	make pvm/complex/dense
	make pvm/dcomplex/dense

install-pvm-include:
	@echo
	@if test $(INCFILE) ; then\
		if test -f $(INCFILE) ; then\
			echo "Copying $(INCFILE) to PVM directories...";\
			cp $(INCFILE) $(EXMPLDIR)/pvm/$(PVMINCLUDE);\
			for dir in $(PVMDIR);\
			do\
				cp $(INCFILE) $(EXMPLDIR)/$$dir/$(PVMINCLUDE);\
			done;\
			for dir in $(PRECISION);\
			do\
				cp $(INCFILE) $(EXMPLDIR)/pvm/$$dir/$(PVMINCLUDE);\
			done;\
			echo "Done.";\
		else\
			echo "File $(INCFILE) does not exist.";\
		fi;\
	else\
		echo "No filename supplied. Usage: make install-pvm-include INCFILE=name-of-fpvm3.h";\
	fi;

clean-pvm-include:
	@echo
	@echo "Removing $(PVMINCLUDE) from PVM directories..."
	@rm -f $(EXMPLDIR)/pvm/$(PVMINCLUDE);\
	for dir in $(PVMDIR);\
	do\
		rm -f $(EXMPLDIR)/$$dir/$(PVMINCLUDE);\
	done;\
	for dir in $(PRECISION);\
	do\
		rm -f $(EXMPLDIR)/pvm/$$dir/$(PVMINCLUDE);\
	done
	@echo "Done."

#
#
# Examples: MPI
#
mpi/single/dense: single
	@echo
	@echo "Making examples/mpi/single/dense"
	@make default WHICHDIR=/mpi/single/dense

mpi/single/pde: single
	@echo
	@echo "Making examples/mpi/single/pde"
	@make default WHICHDIR=/mpi/single/pde

mpi/double/dense: double
	@echo
	@echo "Making examples/mpi/double/dense"
	@make default WHICHDIR=/mpi/double/dense

mpi/double/pde: double
	@echo
	@echo "Making examples/mpi/double/pde"
	@make default WHICHDIR=/mpi/double/pde

mpi/complex/dense: complex
	@echo
	@echo "Making examples/mpi/complex/dense"
	@make default WHICHDIR=/mpi/complex/dense

mpi/dcomplex/dense: dcomplex
	@echo
	@echo "Making examples/mpi/dcomplex/dense"
	@make default WHICHDIR=/mpi/dcomplex/dense

allmpi:
	make mpi/single/dense
	make mpi/single/pde
	make mpi/double/dense
	make mpi/double/pde
	make mpi/complex/dense
	make mpi/dcomplex/dense

install-mpi-include:
	@echo
	@if test $(INCFILE) ; then\
		if test -f $(INCFILE) ; then\
			echo "Copying $(INCFILE) to MPI directories...";\
			for dir in $(MPIDIR);\
			do\
				cp $(INCFILE) $(EXMPLDIR)/$$dir/$(MPIINCLUDE);\
			done;\
			for dir in $(PRECISION);\
			do\
				cp $(INCFILE) $(EXMPLDIR)/mpi/$$dir/$(MPIINCLUDE);\
			done;\
			echo "Done.";\
		else\
			echo "File $(INCFILE) does not exist.";\
		fi;\
	else\
		echo "No filename supplied. Usage: make install-mpi-include INCFILE=name-of-mpif.h";\
	fi;

clean-mpi-include:
	@echo
	@echo "Removing $(MPIINCLUDE) from MPI directories..."
	@for dir in $(MPIDIR);\
	do\
		rm -f $(EXMPLDIR)/$$dir/$(MPIINCLUDE);\
	done;\
	for dir in $(PRECISION);\
	do\
		rm -f $(EXMPLDIR)/mpi/$$dir/$(MPIINCLUDE);\
	done
	@echo "Done."

#
# Cleaning directories
#
singleclean:
	@echo
	@echo "Removing single-precision PIM"
	@if test -f $(SPLIB)/Makefile ; then\
		cd $(SPLIB); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(SPLIB)";\
	fi;
	@if test -f $(SPSRC)/Makefile ; then\
		cd $(SPSRC); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(SPSRC)";\
	fi;

doubleclean:
	@echo
	@echo "Removing double-precision PIM"
	@if test -f $(DPLIB)/Makefile ; then\
		cd $(DPLIB); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(DPLIB)";\
	fi;
	@if test -f $(DPSRC)/Makefile ; then\
		cd $(DPSRC); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(DPSRC)";\
	fi;

complexclean:
	@echo
	@echo "Removing complex PIM"
	@if test -f $(CPLIB)/Makefile ; then\
		cd $(CPLIB); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(CPLIB)";\
	fi;
	@if test -f $(CPSRC)/Makefile ; then\
		cd $(CPSRC); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(CPSRC)";\
	fi;

dcomplexclean:
	@echo
	@echo "Removing double complex PIM"
	@if test -f $(ZPLIB)/Makefile ; then\
		cd $(ZPLIB); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(ZPLIB)";\
	fi;
	@if test -f $(ZPSRC)/Makefile ; then\
		cd $(ZPSRC); make clean;\
	else\
		echo;\
		echo "Error: no Makefile found on $(ZPSRC)";\
	fi;

sequentialclean:
	@echo
	@echo "Removing sequential examples..."
	@for dir in $(SEQDIR);\
	do\
		if test -f $(EXMPLDIR)/$$dir/Makefile ; then\
			cd $(EXMPLDIR)/$$dir; make clean;\
		else\
			echo;\
			echo "Error: no Makefile found on $(EXMPLDIR)/$$dir";\
		fi;\
	done
	@echo "Done."

pvmclean:
	@echo
	@echo "Removing PVM examples..."
	@for dir in $(PVMDIR);\
	do\
		if test -f $(EXMPLDIR)/$$dir/Makefile ; then\
			cd $(EXMPLDIR)/$$dir; make clean;\
		else\
			echo;\
			echo "Error: no Makefile found on $(EXMPLDIR)/$$dir";\
		fi;\
	done
	@echo "Done."

mpiclean:
	@echo
	@echo "Removing MPI examples..."
	@for dir in $(MPIDIR);\
	do\
		if test -f $(EXMPLDIR)/$$dir/Makefile ; then\
			cd $(EXMPLDIR)/$$dir; make clean;\
		else\
			echo;\
			echo "Error: no Makefile found on $(EXMPLDIR)/$$dir";\
		fi;\
	done
	@echo "Done."

examplesclean:
	@echo
	@echo "Removing all examples..."
	@make sequentialclean
	@make pvmclean
	@make mpiclean

makefilesclean:
	@echo
	@echo "Removing makefiles..."
	@for dir in $(PIMDIRS) ;\
	do\
		rm -f $$dir/Makefile ;\
	done
	@for dir in $(EXDIRS) ;\
	do\
		rm -f $(EXMPLDIR)/$$dir/Makefile ;\
	done
	@rm $(COMMON)/Makefile
	@echo "Done."

realclean:
	@echo
	@echo "Removing PIM"
	@make singleclean
	@make doubleclean
	@make complexclean
	@make dcomplexclean
	@make examplesclean
	@make clean-pvm-include
	@make clean-mpi-include
	@make clean-machcons
	@make makefilesclean
