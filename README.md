# PIM 2.3
Parallel Iterative Methods package for Systems of Linear Equations
(Fortran 77 version)
--------------------------------------------------------------------------------

Rudnei Dias da Cunha
  National Supercomputing Centre and Mathematics Institute
  Universidade Federal do Rio Grande do Sul, Brasil

Tim Hopkins
  Computing Laboratory, University of Kent at Canterbury, United Kingdom

--------------------------------------------------------------------------------

## INTRODUCTION
The Parallel Iterative Methods (PIM) is a collection of Fortran routines
designed to solve systems of linear equations (SLEs) on parallel computers
using iterative methods.

PIM offers a number of iterative methods, including
  - Conjugate-Gradients (CG),
  - Conjugate-Gradients for normal equations with minimisation of the
    residual norm (CGNR),
  - Conjugate-Gradients for normal equations with minimisation of the
    error norm (CGNE),
  - Bi-Conjugate-Gradients (Bi-CG),
  - Conjugate-Gradients squared (CGS),
  - The stabilised version of Bi-Conjugate-Gradients (Bi-CGSTAB),
  - Restarted, stabilised version of Bi-Conjugate-Gradients (RBi-CGSTAB)
  - Restarted generalised minimal residual (RGMRES),
  - Restarted generalised conjugate residual (RGCR),
  - Quasi-minimal residual by Buecker and Sauren (QMR),
  - Transpose-free quasi-minimal residual (TFQMR),
  - Chebyshev acceleration (CHEBYSHEV)

The routines allow the use of preconditioners; the user may choose to use
left-, right- or symmetric-preconditioning. Several stopping criteria can also
be chosen.

PIM was developed with two main goals
  1. To allow the user complete freedom with respect to the matrix storage,
access and partitioning;
  2. To achieve portability across a variety of parallel architectures
and programming environments.

These goals are achieved by hiding from the PIM routines the specific details
concerning the computation of the following three linear algebra operations
  1. Matrix-vector (and transpose-matrix-vector) product
  2. Preconditioning step
  3. Inner-product and vector norm

PIM has been tested on networks of workstations using PVM 3 and MPI,
and on supercomputers like the SGI Challenge, Kendall Square Research KSR1,
Cray Y-MP2E/232, Cray C9016E, Cray T3D, Intel Paragon XP/S, Intel iPSC/860,
IBM 9070 SP/1.

**Please read the file "Copyright" for the conditions of use.**

## INSTALLATION VIA CMAKE

PIM can be installed using [CMake](https://cmake.org/). You will need a
Fortran compiler, an MPI installation and the Intel Math Kernel Libraries.
If installing on a machine using [spack](https://spack.io), the following set of
dependencies has been found to work
```
spack load gcc@12.2.0
spack load gmake %gcc@12.2.0
spack load cmake %gcc@12.2.0
spack load mpich %gcc@12.2.0
```

Then, once you have your dependencies loaded, you can configure and build
PIM using
```
cmake -B build -S .
cmake --build build --parallel
```

PIM will build in a `build` folder. If you want to clean up after a build, you
can simply `rm -rf build` on the build folder.

### Configuring the build

To configure the build, you can set options by replacing `cmake -B build -S .` with
```
ccmake -B build -S .
```
or can add options at the command-line, such as
```
cmake -B build -S . -DPIM_DATATYPE=double -DENABLE_SHARED_PIM_LIB=ON -DCMAKE_BUILD_TYPE=Release
```


## INSTALLATION VIA MAKEFILE (original method)

The current distribution contains
 1. The PIM routines in the directories "single", "double", "complex" and
    "dcomplex"
 2. A set of example programs in the directories "examples/sequential",
    "examples/pvm" and "examples/mpi"
 3. A users' guide in compressed PostScript format in the "doc" directory.

After "untaring" the file "pim.tar", change directory to ~/pim and edit the
Makefile. The following variables may need to be modified

* `HOME`      Your top directory, e.g. `/u1/users/fred`
* `FC`        Your Fortran compiler of choice, usually `f77`
* `FFLAGS`    Flags for the Fortran compilation of main programs
             (example programs)
* `OFFLAGS`   Flags for the Fortran compilation of separate
             modules (PIM routines and modules of examples);
             MUST contain at least flag for separate compilation
             (usually `-c`)
* `AR`        The archiver program, usually `ar`
* `HASRANLIB` Either `t` (true) or `f` (false), indicating if it is
             necessary to use a random library program (usually
             ranlib) to build the PIM library
* `BLASLIB`   The command switches for the library containing the
             BLAS routines; for instance, if a library libblas.a
             is installed in a system-wide basis, then set `BLASLIB`
             to `-lblas`
* `PARLIB`    The compilation switches for the parallel libraries. This
             variable should be left blank if PIM is to be used in
             sequential mode. For example, if p4 1.2 has been installed
             in the user's area, then PARLIB  would be defined as
             `-L $(HOME)/p4-1.2/lib -lp4 -L $(HOME)/p4-1.2/lib -lp4`


### Before building PIM

PIM needs the values of some machine-dependent floating-point constants.
The single- or double-precision values are stored in the files
`pim/common/smachcons.f` and `pim/common/dmachcons.f` respectively.
Default values are supplied for the IEEE-754 floating-point standard,
and are stored separately in the files `pim/common/smachcons.f.ieee754`
and `pim/common/dmachcons.f.ieee754` -- these are used by default. However
if you are using PIM on a computer which does not support the IEEE-754
standard, you may:

1. type
		`make smachcons`
	   or
		`make dmachcons`
	   This will compile and execute a program which uses the
	   LAPACK routine _LAMCH, to compute those constants, and the
	   files pim/common/smachcons.f or pim/common/dmachcons.f
	   (whichever is appropriate) will be generated.

2. edit either `pim/common/smachcons.f.orig` or
	   `pim/common/dmachcons.f.orig` and replace the strings
	   `MACHEPSVAL`, `UNDERFLOWVAL` and `OVERFLOWVAL` by the values of
	   the machine epsilon, underflow and overflow thresholds to
	   those of the particular computer you are using, either in
	   single- or double-precision.


### To build PIM

Type `make makefiles` to build the makefiles in the appropriate directories and then

* `make single`
* `make double`
* `make complex`
* `make dcomplex`

to build the single-precision, double-precision, complex or double complex
versions of PIM. This will generate `.o` files, one for each iterative method
routine and also `libpim.a` which contains the support routines.


### Before building the examples

1) edit the file `examples/common/timer.f` and modify it to use
the timing function available in your machine.  Examples are provided
in the source file for the Cray, IBM RS/6000 and the UNIX etime
function. By default, the latter is used.

2) The PVM and MPI example programs use the Fortran INCLUDE
statement to include the PVM and MPI header files. Some compilers have
a switch (usually `-I`) which allows the user to provide search
directories in which files to be included are located (as with the IBM
AIX XL Fortran compiler); while others require the presence of those
files in the same directory as the source code resides. In the first
case, you will need to include in the FFLAGS variable the relevant
switches (see INSTALLATION); in the latter, you will need to install
the PVM and MPI header files (`fpvm3.h` and `mpif.h` respectively) by
typing
```
	make install-pvm-include INCFILE=<name-of-fpvm3.h> make
	install-mpi-include INCFILE=<name-of-mpif.h>
```
where you should replace `<name-of-fpvm3.h>` and `<name-of-mpif.h>` by the
full filename of the required include files; for instance, if PVM is
installed on /usr/local/pvm3 then you should type
```
	make install-pvm-include INCFILE=/usr/local/pvm3/include/fpvm3.h
```

The example programs are in the directories

* `examples/sequential/single/dense`
* `examples/sequential/single/pde`
* `examples/sequential/single/harwell-boeing`
* `examples/sequential/single/pvp-pde`

* `examples/sequential/double/dense`
* `examples/sequential/double/pde`
* `examples/sequential/double/harwell-boeing`

* `examples/sequential/complex/dense`

* `examples/sequential/dcomplex/dense`


* `examples/pvm/single/dense`
* `examples/pvm/single/pde`

* `examples/pvm/double/dense`
* `examples/pvm/double/pde`

* `examples/pvm/complex/dense`

* `examples/pvm/dcomplex/dense`


* `examples/mpi/single/dense`
* `examples/mpi/single/pde`

* `examples/mpi/double/dense`
* `examples/mpi/double/pde`

* `examples/mpi/complex/dense`

* `examples/mpi/dcomplex/dense`

To build the examples, type make followed by the name of a subdirectory of
examples, e.g.
```
	make sequential/single/dense
```
The example programs can also be built locally in those directories by
changing to a specific directory and typing make.

Please read the file `~/pim/examples/Readme` for more information.

### CLEANING UP
The object files in the single, double, complex, dcomplex and examples
directories can be cleaned by typing

* `make singleclean`
* `make doubleclean`
* `make complexclean`
* `make dcomplexclean`
* `make pvmclean`
* `make mpiclean`
* `make clean-machcons`
* `make clean-pvm-include`
* `make clean-mpi-include`
* `make examplesclean`
* `make makefilesclean`

You can revert the package to its original form (except for the
top level Makefile) by typing
```
	make realclean
```

### USING PIM
To use PIM with your application, link your program with the `.o` file
corresponding to the PIM iterative method routine being called and with the PIM
support library libpim.a .

### USING THE BLAS
Some systems offer highly optimised versions of the BLAS which you are
encouraged to use for better performance. If the system you are using
does not have the BLAS available, you can retrieve the routines from
the many sites running NETLIB. The BLAS routines that will be needed
for the PIM routines are
```
  SSCAL/DSCAL/CSCAL/ZSCAL
  SCOPY/DCOPY/CCOPY/ZCOPY
  SAXPY/DAXPY/CAXPY/ZAXPY
  SDOT/DDOT/CDOTC/ZDOTC
  STRSV/DTRSV/CTRSV/ZTRSV
```
and for the examples you will need
```
  SNRM2/DNRM2/SCNRM2/DZNRM2
  SGEMV/DGEMV/CGEMV/ZGEMV
  LSAME
  XERBLA
```
You may compile these and generate a library called `libblas.a` using
the following commands
```
  f77 -O -c *.f
  ar rcv libblas.a *.o
  ranlib libblas.a
  rm *.o
```

Notes:
  - Replace f77 for the Fortran compiler of your choice
  - Some systems use a different archiver program instead of ar
  - Some systems do not require the use of ranlib

## COMMENTS/SUGGESTIONS
The authors can be contacted via e-mail at either rcunha@mat.ufrgs.br
or trh@ukc.ac.uk
